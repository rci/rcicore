#include <signal.h>
#include "daemonize.h"

/** @todo _workingDirectory and _lockDirectory should be defined at compilation by autotools*/
int             Rci::Daemonize::_nAccessors;
std::string     Rci::Daemonize::_workingDirectory;
std::string     Rci::Daemonize::_lockDirectory("/var/lock/");
mode_t          Rci::Daemonize::_mask; 
bool            Rci::Daemonize::_daemonized;
std::string     Rci::Daemonize::_name;
boost::mutex    Rci::Daemonize::_mutex;

/** This constructor configures the BF_Daemon
 *  @param[in] inName : name of the daemon, used for the shared memory
 *  @param[in] inWorkingDirectory : working directory of the daemon
 *  @param[in] inMask : mask used to set the right of the file created by the daemon
 */
Rci::Daemonize::Daemonize(std::string inName, std::string inWorkingDirectory, mode_t inMask) :
    _stop(false), _restart(false), _alive(false), _logger("Daemonizer"), _shmDaemon(NULL),
    _mtxTX(NULL), _newTX(NULL), _TX(NULL), _mtxRX(NULL), _newRX(NULL), _RX(NULL), _pid(0)
{
  {
    boost::unique_lock<boost::mutex> lk(_mutex);
    if(_nAccessors == 0)
    {
        _name = inName;
        _daemonized = false;
        _workingDirectory = inWorkingDirectory;
        _mask = inMask;
    }
    ++ _nAccessors;
  }  
    
  checkExisting();
}

Rci::Daemonize::~Daemonize()
{
  {
    boost::unique_lock<boost::mutex> lk(_mutex);
    if( (--_nAccessors == 0) && ( _daemonized )) 
        removeSharedMemory();
  }
}

/** This function will change the program to a daemon.
 *  @param[in] inFake : if true, the program is not daemonized.
 *  @return positive or null value on success, negative value on failure
 */
int Rci::Daemonize::daemonizeMe( bool inFake )
{
  int ans = 0;
  if( checkExisting() )
      ans = -1;   // another instance is already running
  else if( _daemonized )
      ans = 1;    // this instance is already daemonized
  else
  {
    if( checkExisting() == false )
    {
      int i = 0;
      if( inFake == false )
      {
        // Run a child instance of the programm and kill the parent
        // The child is then related to the init process (pid 1)
        // It's now a daemon.
        i=fork();
        if (i<0) ans = -1; // fork error (fork<0)
        if (i>0) exit(0);  // Kill Parent

        // Put the child in a new group. Consequently, the programm will not depend on things happening in its parent group's
        // Such as closing the terminal
        if( ans >= 0)
          ans = setsid();

        // Close opened descriptors inherited from the parent
        if( ans >= 0 )
        {
          for (i=getdtablesize(); i>=0; --i)
          {
            close(i);
          }
        }
      
        // Open stdin, stdout and stderr descriptors for the daemon in a safe IO device (/dev/null)
        if( ans >= 0 )
        {
          i=open("/dev/null",O_RDWR); // open stdin
          if( i >= 0 )
            ans = dup(i); // open stdout
          else
            ans = -1;
            
          if( ans >= 0 )
            ans = dup(i); // open stderr
        }

        // Defines the default permission mask for files created by the daemon
        // the mask is the complement of argument sent to umask
        if( ans >= 0 )
          umask(_mask);
      }

      // Sets the work directory of the programm.
      // The daemon will be independent of the directory from which his parent was started
      if( ans >= 0 )
      {
        if(_workingDirectory == "")
            _workingDirectory = get_current_dir_name();
        else
            ans = chdir(const_cast<char*>(_workingDirectory.c_str()));
      }

      if( ans < 0 )
      {
        _error.assign(errno, boost::system::system_category());
        std::ostringstream ssMessage;
        ssMessage << "error " << _error.category().name() <<" n°" << _error.value() << " " << _error.message();
        _logger.logIt(ssMessage, LgCpp::Logger::logWARNING);
      }
      
      if( ans >= 0 )
      {
        /* Creates a lock file which will be checked by other processes to know wether the daemon is running or not
         */
        if( ans >= 0 )
        {
          std::string lockFile(_lockDirectory + _name);
          lockFile += ".lock";
        	i=open(const_cast<char*>(lockFile.c_str()),O_RDWR|O_CREAT,0644);
          if (i<0)	// the file could not be opened
            // the lock file could not be opened nor created
            ans = -1;
        }
        if( ans >= 0 )
        {
        	// Tries to lock the file
          if (lockf(i,F_TLOCK,0)<0)
            ans = -1;
        }
        if( ans < 0 )
        {
          _error.assign(errno, boost::system::system_category());
          std::ostringstream ssMessage;
          ssMessage << "error " << _error.category().name() <<" n°" << _error.value() << " " << _error.message();
          _logger.logIt(ssMessage, LgCpp::Logger::logWARNING);
        }
        if( ans >= 0 )
        {
          if( createSharedMemory() == 0)
            _daemonized = true;
          else
            ans = -1;
        }
      }
    }
    else
    {
      ans = -1;
    }
  }
  return ans;
}


bool Rci::Daemonize::checkNULL()
{
  return (_newRX == NULL) || (_RX == NULL) || (_mtxRX == NULL) || (_newTX == NULL) || (_TX == NULL) || (_mtxTX == NULL);
}

/** checks if the daemon already exists
 *  @return true if the program or daemon exists
 */
bool Rci::Daemonize::checkExisting()
{
    bool ans = true;
    bool shmExists = false;
    // try to open the shared memory. If it fails, no daemon exists.
    try
    {
      // on success, should check if the daemon is really running (if the daemon stoped on failure, the shared memory
      // could still exists)
      if( _shmDaemon == NULL )
        _shmDaemon = new boost::interprocess::managed_shared_memory(boost::interprocess::open_only, _name.c_str(), NULL);
      
      shmExists = true;
      
      // check if the process with pid = testPid exists
      int testPid = *(_shmDaemon->find<int>( "PID" ).first);

      if(kill(testPid, 0) == -1)
      {
          switch(errno)
          {
              case(EPERM):
              {
                  // no permission to access this program.
                  // we will check if this program is our daemon later
                  break;
              }
              case(ESRCH):
              {
                  // the program with pid = testPid does not exists.
                  ans = false;
                  break;
              }
              default:
                  break;
          }
      }
      
      // a process with pid = testPID exists, but we actually don't know if it is our daemon
      if(ans == true)
      {
        std::string lockFile(_lockDirectory + _name);
        lockFile += ".lock";
      	int i=open(const_cast<char*>(lockFile.c_str()),O_RDWR|O_CREAT,0644);
        if( i >= 0 )
        {
        	// Tries to lock the file
          if (lockf(i,F_TEST,0)<0)
          {
            if( errno == EAGAIN || errno == EACCES )
              ans = true;
            else
              ans = false;
          }
          else
            ans = false;
          close(i);
        }
        else
          ans = false;

        if(ans == true)
          _pid = testPid;
      }
      
    }
    catch(boost::interprocess::interprocess_exception intEx)
    {
      ans = false;
    }
    // the shared memory exists, but not our daemon. We remove it.
    if( (ans == false) && (shmExists == true))
        removeSharedMemory();
    
    return ans;
}

int Rci::Daemonize::createSharedMemory()
{
  int ans = 0;
  // try to create the shared memory. If it fails, -1 is returned.
  try
  {
    // creates the memory if the shared memory does not exists
    if( _shmDaemon == NULL )
    {
      _logger.logIt("creating shared memory", LgCpp::Logger::logDEBUG);
      
      _shmDaemon = new boost::interprocess::managed_shared_memory( boost::interprocess::create_only, _name.c_str(), 1024 );
      
      _shmDaemon->construct<int>("PID")( getpid() );

      _mtxTX = _shmDaemon->construct<boost::interprocess::interprocess_mutex>("mtxToClient")();
      _TX = _shmDaemon->construct<string>("ToClient")( "I'm alive", _shmDaemon->get_segment_manager() );
      _newTX = _shmDaemon->construct<bool>("NEW_ToClient")( true );

      _mtxRX = _shmDaemon->construct<boost::interprocess::interprocess_mutex>("mtxToServer")();
      _RX = _shmDaemon->construct<string>("ToServer")( "", _shmDaemon->get_segment_manager() );
      _newRX = _shmDaemon->construct<bool>("NEW_ToServer")( false );
    }
  }
  catch(boost::interprocess::interprocess_exception intEx)
  {
      _logger.logIt("error while creating shared memory", LgCpp::Logger::logDEBUG);
      ans = -1;
  }
  return ans;
}

int Rci::Daemonize::initSharedPtr()
{
  int ans = 0;
  
  if( checkNULL() )
  {
    // try to open the shared memory. If it fails, -1 is returned.
    try
    {
      _logger.logIt( "initializing shared pointers", LgCpp::Logger::logDEBUG );
      
      if( _shmDaemon == NULL )
        _shmDaemon = new boost::interprocess::managed_shared_memory(boost::interprocess::open_only, _name.c_str(), NULL);
      
      _logger.logIt( "shared memory attached", LgCpp::Logger::logDEBUG );
      
      if( _mtxRX == NULL )
        _mtxRX = _shmDaemon->find<boost::interprocess::interprocess_mutex>("mtxToClient").first;
      if( _RX == NULL )
        _RX = _shmDaemon->find<string>( "ToClient" ).first;
      if( _newRX == NULL )
        _newRX = _shmDaemon->find<bool>( "NEW_ToClient" ).first;

      if( _mtxTX == NULL )
        _mtxTX = _shmDaemon->find<boost::interprocess::interprocess_mutex>("mtxToServer").first;
      if( _TX == NULL )
        _TX = _shmDaemon->find<string>( "ToServer" ).first;
      if( _newTX == NULL )
        _newTX = _shmDaemon->find<bool>( "NEW_ToServer" ).first;
    }
    catch( boost::interprocess::interprocess_exception intEx )
    {
      std::string message = "initSharedPtr ";
      message += intEx.what();
      _logger.logIt( message, LgCpp::Logger::logERROR );
      ans = -1;
    }
  }
  
  if( checkNULL() )
  {
    _logger.logIt("could not access shared objects", LgCpp::Logger::logERROR);
    ans = -1;
  }
  
  return ans;
}

/** Send a message to the running daemon/program
 *  @param[in] inMessage : message to send
 *  @return -1 on failure
 */
int Rci::Daemonize::post( std::string inMessage )
{
  int ans = 0;
  
  ans = initSharedPtr();
  // try to write in the shared memory.
  if( ans == 0 )
  {
    _logger.logIt( inMessage, LgCpp::Logger::logINFO );
    
    _mtxTX->lock();
    ///\todo I have to write the data twice... to allow the redimensionning of the string.
      *_TX = inMessage.c_str();
      *_TX = inMessage.c_str();
      *_newTX = true;
    _mtxTX->unlock();
    kill(_pid,SIGALRM);
  }
  
  return ans;
}

/** Read a message to the running daemon/program
 *  @param[out] outMessage : readed message
 *  @return -1 on failure
 */
int Rci::Daemonize::read( std::string &outMessage )
{
  int ans = 0;
  
  ans = initSharedPtr();
  // try to read the shared memory.
  if( ( ans == 0 ) && ( *_newRX ) )
  {
    ///\todo add mutex
    _mtxRX->lock();
      outMessage = _RX->c_str();
      *_newRX = false;
    _mtxRX->unlock();

    _logger.logIt( outMessage, LgCpp::Logger::logINFO );
    
    if( outMessage == "stop" )
      _stop = true;
    else if( outMessage == "restart" )
      _restart = true;
    else if( outMessage == "who" )
    {
      _logger.logIt( "who message received", LgCpp::Logger::logDEBUG );
      post( _name );
    }
    else if( outMessage == _name )
    {
      _logger.logIt( "daemon is alive", LgCpp::Logger::logDEBUG );
      _alive = true;
    }
  }

  return ans;
}
/** removes the shared memory
 *  @return -1 on error, 0 if the shared memory was removed, 1 if the shared memory could'nt be removed
 *  @note   if 1 is returned, the shared memory is used by an other process and couldn't be removed
 */
int Rci::Daemonize::removeSharedMemory()
{
    int ans = 0;
    // try to remove the shared memory. If it fails, -1 is returned.
    try
    {
      if( _shmDaemon != NULL )
        delete _shmDaemon;
      _shmDaemon = NULL;
      
      ans = boost::interprocess::shared_memory_object::remove( _name.c_str() )? 0:1;
      if(ans == 0)
        _logger.logIt( "shared memory removed", LgCpp::Logger::logINFO );
      else
        _logger.logIt( "shared memory could not be removed", LgCpp::Logger::logINFO );
    }
    catch(boost::interprocess::interprocess_exception intEx)
    {
      _logger.logIt("error while removing the shared memory", LgCpp::Logger::logERROR);
      ans = -1;
    }
    return ans;
}

