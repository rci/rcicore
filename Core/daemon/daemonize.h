/** This file defines a daemon object. This object is used to handle daemon parameters and, communication slots
 */

#ifndef __DAEMONIZE_H__
#define __DAEMONIZE_H__

#include <sys/types.h>
#include <sys/stat.h>
#include <string>
#include <boost/thread.hpp>
#include <boost/system/error_code.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/string.hpp> 
#include <logcplusplus.hpp>

#ifndef WORKING_PATH
#define WORKING_PATH "."
#endif

namespace Rci
{
/** @brief Class used to create the daemon
 *
 *  This class handles the actions to daemonize the program
 */
class Daemonize
{
private:
    typedef boost::interprocess::allocator<char, boost::interprocess::managed_shared_memory::segment_manager> CharAllocator; 
    typedef boost::interprocess::basic_string<char, std::char_traits<char>, CharAllocator> string; 
    boost::interprocess::managed_shared_memory* _shmDaemon; ///< shared memory

private:
    static std::string _workingDirectory;   ///< the working directory of the daemon
    static std::string _lockDirectory;      ///< the directory where the lock file will be created
    static mode_t _mask;    ///< used to set the rights of files created by the daemon
    static bool _daemonized;    ///< true if the program runs as a daemon
    static std::string _name;   ///< daemon Name
    static int _nAccessors; ///< number of accessors
    static boost::mutex _mutex;

private:    
    int _pid; ///< pid of the daemon
    
    boost::system::error_code _error;
    
    bool _stop;     ///< true if the daemon must stop
    bool _restart;  ///< true if the daemon must restart
    bool _alive;    ///< set to true if the daemon answered to «who»
    
    boost::interprocess::interprocess_mutex *_mtxTX;  ///< mutex protecting TX data
    bool *_newTX;   ///< new Message sent
    string *_TX;    ///< message to send
    boost::interprocess::interprocess_mutex *_mtxRX;  ///< mutex protecting RX data
    bool *_newRX;   ///< new Message to read
    string *_RX;    ///< message to read
    
    LgCpp::Logger _logger; ///< daemonizer logger

public:
    Daemonize( std::string inName, std::string inWorkingDirectory = "", mode_t in_Mask = 0111 );
    ~Daemonize();
    
    int daemonizeMe( bool fake = false );    ///< used to run as daemon
    bool checkExisting(); ///< this function checks if the program is already started
    
    int post( std::string inMessage );  ///< sends a message to the main instance
    int read( std::string &outMessage ); ///< reads incomming messages
    
    std::string getWorkingDirectory() const {return _workingDirectory;}///< returns the working directory
    mode_t getMask() const {return _mask;}  ///< returns the mask
    bool getStop() {bool ans = _stop; _stop = false; return ans;} ///< returns stop flag
    bool getRestart() {bool ans = _restart; _restart = false; return ans;}  ///< returns the restart flags

private:
    bool checkNULL();           ///< true if a single memory pointer is NULL
    int createSharedMemory();   ///< creates the shared memory for the daemon
    int removeSharedMemory();   ///< removes the shared memory used by the daemon
    int initSharedPtr();        ///< initialize the shared pointers: _message, _newMessage
    int handleMessages();       ///< handles the received messages
};
}
#endif
