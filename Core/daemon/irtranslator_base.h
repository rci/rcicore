/** @file irtranslator_base.h
 *  @brief This file defines the default translator class
 *  
 *  Include this file to create your own translator. This class was originally written to decode lirc mode2
 *  infra-red data and therefore contains specific functions. However, it can be used for other kind of
 *  data since the main function can be overriden.
 */

#ifndef _IRTRANSLATOR_BASE_H_
#define _IRTRANSLATOR_BASE_H_

#include <list>
#include <vector>
#include <iostream>
#include <logcplusplus.hpp>

namespace Rci
{
/** @brief Base class for command translator.
 *
 *  include this file to create your own loadable translators.
 */
class IrTranslator_Base
{
public:
  /// Structure defining an IR data
  struct IrCode
  {
    bool _pulseState; ///< true if the data is a pulse, false if it is a space
    unsigned int _length; ///< pulse/space duration in µs
  };
  
  /// defines the different states of the IR translator
  enum TranslateState
  {
    _bufferEmpty,    ///< the input buffer is empty AND data are missing to validate the data
    _dataOk,         ///< the data was correctly translated
    _invalidData     ///< the data is not valid for this translator.
  };
  
  /** Base Constructor
   *  @param[in] inName : name of the translator.
   */
  IrTranslator_Base(std::string inName) : _logger(inName), _rawCode(0), _code(""), _oldCode(""),
                                          _repeated(false), _isValid(0) {;}
  /// Base Destructor
  virtual ~IrTranslator_Base() {;}
  
  /** translates the prepared data to an unsigned integer corresponding to an IR Code
   *  @param[in] inPreparedData : Data prepared and checked for translation.
   *  @param[in] startPosition : start position of the translation.
   *  @return the actual state of the translator
   */
  virtual TranslateState translate(const std::vector<IrCode> &inPreparedData, int startPosition = 0) = 0;

  /// returns the name of the translator class
  virtual std::string getName() {return "IrTranslator_Base";}

  std::string getCode() {return _code;} ///< returns the last translated code
  unsigned int getStopPosition() {return _stopPosition;} ///< return the current position in the raw data
  bool getRepeated() {return _repeated;}///< returns the repeated flag
  bool getValid() {return _isValid;}    ///< return the isValid flag

  /** prepare the Mode2 data for translation by the translators
   *  @param[in] inIRMode2Data : Lirc Mode2 data to translate.
   *  @param[out] outPreparedData : Data prepared
   *  @return 0 on success, opposit of the position of the first invalid data.
   */
  static int checkAndPrepareData(const std::list<unsigned int> &inIRMode2Data, std::vector<IrCode> &outPreparedData)
  {
    int ans = 0;
    
    outPreparedData.erase(outPreparedData.begin(), outPreparedData.end());
    outPreparedData.reserve(inIRMode2Data.size());
    
    std::list<unsigned int>::const_iterator itMode2;
    int i = 0;
    bool ok = true;
    
    for(itMode2 = inIRMode2Data.begin(); ( itMode2 != inIRMode2Data.end() ) && ok; ++itMode2)
    {
      outPreparedData.push_back(convertMode2(*itMode2));
      // Checks that the pulse is the opposit state of the previous one
      if(i != 0)
      {
        ok = ( (outPreparedData[i])._pulseState != (outPreparedData[i - 1])._pulseState );
      }
      ++i;
    }

    if (ok)
      ans = 0;
    else
      ans = -i;
        
    return ans;
  }
  
  /** Mode2 to IrCode convertor
   *
   *  This function is used to interpret Lirc Mode2 IR data to the IrCode Class.
   *  @param[in] inMode2: Data to convert
   */
  static IrCode convertMode2(unsigned int inMode2)
  {
    IrCode outValue;
    
    outValue._pulseState = true;
    if( ( 0xFF000000 & inMode2) == 0 )
      outValue._pulseState = false;
    
    outValue._length = 0xFFFFFF & inMode2;
    
    return outValue;
  }

protected:
  /** sets the code readed by the translator.
   *  @param[in] inCode : value to write
   */
  virtual void setCode(std::string inCode)
  {
    _oldCode = _code;
    _code = inCode;

    if(_oldCode != _code)
      _repeated = false;
    else
      _repeated = true;
  }
  
  LgCpp::Logger _logger; ///< translator logger.
  
  unsigned int _tmpCode; ///< temporary decoded value.
  unsigned int _rawCode; ///< raw code
  unsigned int _stopPosition; ///< number of data readed until stopped
  bool _repeated;     ///< true if same code was received several times
  bool _isValid;      ///< true if the input code data was correctly decoded

  std::string _code; ///< code
  std::string _oldCode; ///< previous code
};
}
#endif
