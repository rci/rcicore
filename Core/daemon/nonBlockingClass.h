#ifndef _NONBLOCKINGCLASS_H_
#define _NONBLOCKINGCLASS_H_

#include <list>
#include <boost/thread.hpp>
#include <boost/function.hpp>
#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

namespace Rci
{
/// Class defined for convinience to asynchronously handle functions
class NonBlockingClass
{
public:
  NonBlockingClass() : _thread(NULL), _io(), _ioWork(_io)
  {
    _thread = new boost::thread( boost::bind(&boost::asio::io_service::run, &_io) );
  }
  ~NonBlockingClass()
  {
    stop();
  }
  /** add a function to the function queue
   *
   *  The input function is added at the end of the function queue
   *  @param[in] inFunction: function to run
   */
  void addFunction( boost::function<void (void)> inFunction )
  {
    _io.post(inFunction);
  }
  
  void stop()
  {
    _io.stop();
    if( _thread != NULL )
    {
      _thread->join();
      delete _thread;
      _thread = NULL;
    }
  }
  
  /** returns the asio service used by the class
   *  @return the io service used by the class.
   */
  boost::asio::io_service& getIoService()
  {
    return _io;
  }

private:
  boost::asio::io_service _io;
  boost::asio::io_service::work _ioWork;

  boost::thread* _thread; ///< main thread
};

}
#endif
