#include "rciDaemon.h"
#include "plugin_Daemon.h"

Rci::Plugin_Base *Rci::Plugin_Daemon::maker()   ///< Factory
{
    Rci::Plugin_Base *ans = new Rci::Plugin_Daemon;
    return ans;
}

void Rci::Plugin_Daemon::eraser(Rci::Plugin_Base *in_Ptr)   ///< Factory
{
    delete in_Ptr;
}

int Rci::Plugin_Daemon::go_To_State( const std::string &inState, bool inRepeat,
                                     const CmdArguments & inArguments, std::string &outAction,
                                     CmdAnswer &outAnswers )
{
  outAction = "";
  if( checkRepeatAction( inState, inRepeat ) )
  {
    if (inState == "getPlugins")
    {
      if( _daemon != NULL )
        outAnswers = _daemon->getPluginsName();
    }
  }
  return 0;
}

std::string Rci::Plugin_Daemon::getDaemonState()
{
    if(_daemon != NULL) return _daemon->getState();
    else return "Stopped";
}

int Rci::Plugin_Daemon::get_Key_Value( const std::string &inKey, std::string &outValue )
{
  int ans = 0;
  
  if( inKey == "state" )
    outValue = getDaemonState();
  else
    ans = -1;
  return ans;
}

