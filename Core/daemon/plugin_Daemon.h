/** @file plugin_Daemon.h
 *  This plugin is used to control the daemon
 *  @author Matthieu Charbonnier
 */

#ifndef __PLUGIN_DAEMON_H__
#define __PLUGIN_DAEMON_H__
#include "plugin_base.h"
#include "rciDeviceBase.h"

namespace Rci
{
class RciDaemon;

/** @brief Plugin allowing a direct communication with the daemon.
 *
 *  This plugin is always loaded.
 */
class Plugin_Daemon :
    public Plugin_Base
{
public:
    static Rci::Plugin_Base *maker();   ///< Factory
    static void eraser(Rci::Plugin_Base *in_Ptr);   ///< Factory

    Plugin_Daemon(): Plugin_Base("Daemon_Plugin", eraser), _daemon(NULL), _device(NULL)
      {;}
    ~Plugin_Daemon(){;}
    
    int get_Key_Value(const std::string &inKey, std::string &outValue); ///< return a value associated te the given key
    int go_To_State(const std::string &inState, bool inRepeat, const CmdArguments & inArguments,
                    std::string &outAction, CmdAnswer &outAnswers); ///< call the function associated to a given state
    
    /** set a device to control
     *  
     *  This function is used to set a device that will be turned on/off by the plugin. Only one device can
     *  be handled.
     *  @todo this function should be removed or improved to handle more devices
     */
    void setDevice(RciDeviceBase *inDevice) { _device = inDevice; }
    /** sets the daemon controlling the plugin
     *
     *  The daemon will be used to get some system data such as the name of the available plugins.
     */
    void setDaemon(RciDaemon *inDaemon) { _daemon = inDaemon; }
private:
    std::string _state; ///< Daemon State
    RciDeviceBase *_device; ///< the device to switch on/off
    RciDaemon *_daemon; ///< the daemon controlling this plugin
    
    /** Returns the state of the daemon
     */
    std::string getDaemonState();
};
}

#endif
