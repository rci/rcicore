//#include <stdio.h>
//#include <stdlib.h>
#include <iostream>
#include <dlfcn.h>
#include "plugin_base.h"

Rci::Plugin_Base* Rci::Plugin_Base::_runningPlugin = NULL;
boost::mutex Rci::Plugin_Base::_mutexPlugin;   ///< Protects the plugin_base datas
Rci::RciDaemonBase* Rci::Plugin_Base::_daemon = NULL;  ///< Daemon to use to communicate with plugins

std::vector<std::string> Rci::Plugin_Base::_keys;
std::vector<std::string> Rci::Plugin_Base::_status;

/** create a plugin using the inPluginName parameter
 *  @param inPluginName : name of the plugin to create
 *  @return 0 on success, -1 on failure
 *
 *  @todo include in rciDaemon
 */
Rci::Plugin_Base* Rci::Plugin_Base::createPlugin(const std::string &inPluginName)
{
  Rci::Plugin_Base* pluginAns = NULL;
  LgCpp::Logger logger("Plugin Loader");
  
  std::ostringstream ssMessage;
  char* error = NULL;

  std::string tmp_PluginPath(PLUGINS_PATH);
  tmp_PluginPath += "/";
  tmp_PluginPath += inPluginName;
  tmp_PluginPath += ".so";
    
  Plugin_Base* (*maker)( void ) = NULL;
  void (*tmpEraser)( Rci::Plugin_Base* ) = NULL;
  // Opens the corresponding library, notice that one can call several times dlopen on the same library.
  void * pluginHandler = dlopen(tmp_PluginPath.c_str(), RTLD_LAZY);
  if( pluginHandler == NULL )
  {
    // could not open the library
    ssMessage.str("");
    ssMessage << "error n°" << dlerror() << "could not open the library";
    logger.logIt(ssMessage, LgCpp::Logger::logERROR);
  }
  else
  {
    // library successfully opened
    dlerror();  // clear any previous error
    maker = (Rci::Plugin_Base* (*)())dlsym(pluginHandler, "maker");
    error = dlerror();
    if(error != NULL)
    {
      maker = NULL;
      // symbol not found
      ssMessage.str("");
      ssMessage << "error n°" << error << " symbol \"maker\" not found";
      logger.logIt(ssMessage, LgCpp::Logger::logERROR);
    }
    
    tmpEraser = (void (*)(Rci::Plugin_Base*))dlsym(pluginHandler, "eraser");
    error = dlerror();
    if(error != NULL)
    {
      // symbol not found
      ssMessage.str("");
      ssMessage << "error n°" << error << " symbol \"eraser\" not found";
      logger.logIt(ssMessage, LgCpp::Logger::logERROR);
    }
  }
  
  if(maker != NULL)
  {
    pluginAns = maker();
    pluginAns->setEraser(tmpEraser);
  }
  else
  {
    if(error != NULL)
      delete error;
  }
  return pluginAns;
}

/** Base plugin constructor.
 */
Rci::Plugin_Base::Plugin_Base(const std::string& inPluginName)  : _plugin_Name(inPluginName), _requestSysPage(false), _lastState(""), _logger(inPluginName), _systemName("system"), eraser(NULL)
{
  initPlugin();
}

/** Base plugin constructor.
 */
Rci::Plugin_Base::Plugin_Base(const std::string& inPluginName, void (*inEraser)( Rci::Plugin_Base* ))  : _plugin_Name(inPluginName), _requestSysPage(false), _lastState(""), _logger(inPluginName), _systemName("system"), eraser(inEraser)
{
  initPlugin();
}

/** checks if the interval time has elapsed since the last test
 *  @param[in,out] inOutLastTime : time of the last action
 *  @param[in] inInterval : interval time to check
 *  @param[in] updateIfTrue : if true update inOutLastTime to actual time if the time has elapsed, if false, do the contrary
 *  @return true if the inInterval time has elapsed
 */
bool Rci::Plugin_Base::checkTimer( timeval &inOutLastTime, timeval const& inInterval, bool updateIfTrue )
{
    timeval newTime, elapsedTime;
    gettimeofday(&newTime, NULL);
    
    timersub( &newTime, &inOutLastTime, &elapsedTime);
    
    bool ans = (timercmp( &elapsedTime, &inInterval, > ) != 0);
    
    if(ans == updateIfTrue)
    {
        inOutLastTime.tv_sec = newTime.tv_sec;
        inOutLastTime.tv_usec = newTime.tv_usec;
    }
    return ans;
}

/** Adds an action associated to an IR command to the plugin
 *  @param[in] inAction : Name of the action to perform
 *  @param[in] inTranslatorName : Name of the translator associated to the command
 *  @param[in] IRValue : value of the command
 *  @param[in] inArguments : arguments of the command
 */
int Rci::Plugin_Base::addCommand(const std::string &inAction, const std::string &inTranslatorName,
               const std::string &IRValue, CmdArguments &inArguments)
{
  RcCode rcCode( IRValue, inTranslatorName, inArguments );
  _actionsMap.insert( std::pair<RcCode, std::string>( rcCode, inAction ));
  return 0;
}

/** Asks the plugin to treat an incomming IR command
 *  @param[in] inCode : Device rc code
 *  @param[out] outAction : action to perform with the given answers
 *  @param[out] outAnswers : answers of the action associated to inCode
 *  @return 0
 */
int Rci::Plugin_Base::handleCommand( RcCode &inCode, std::string &outAction, CmdAnswer &outAnswers )
{
  int ans = 0;
  
  std::map<RcCode, std::string>::iterator it = _actionsMap.find( inCode );
  if( it != _actionsMap.end() )
  {
    //uses command arguments if present...
    if( inCode.getCommandArguments().size() != 0 )
      go_To_State( it->second, inCode.getIsNew(), inCode.getCommandArguments(), outAction, outAnswers );
    // ... or default ones if not.
    else
      go_To_State( it->second, inCode.getIsNew(), it->first.getCommandArguments(), outAction, outAnswers );
  }
  else
    ans = -1;
  return ans;
}

/** check if the action passed as parameter must be repeated or not
 *  @param[in] inState : state/action to perform
 *  @param[in] inAutoRepeat : true if the programm must auto repeat the action
 *  @return true if the action must be performed
 */
bool Rci::Plugin_Base::checkRepeatAction( const std::string &inState, bool inAutoRepeat )
{
    std::ostringstream ssMessage;
    bool ans = false;
    if( ( inState != _lastState ) || (!inAutoRepeat) )
       ans = true;
    else
    {
        // checks that the last action occurs enough time ago (200ms).  
        timeval newTime, elapsedTime;
        gettimeofday(&newTime, NULL);
        
        timersub( &newTime, &_lastTime, &elapsedTime);
        
        if( timercmp( &elapsedTime, &_timeInterval, > ) != 0 )
            ans = true;
    }
    
    if (ans)
    {
        set_TimeInterval( _defaultInterval );
        gettimeofday(&_lastTime, NULL);
        _lastState = inState;
    }
    
    return ans;
}

/** returns the page containing the data of the plugin
 *  @return 0 on success, -1 on error
 */
int Rci::Plugin_Base::p_updatePage()
{
  int ans = 0;
  std::list<RciOutpage>::iterator tmpRciOutpage;
  std::string parameterName, parameterValue;
  
  for(tmpRciOutpage = _pageToDisplay.begin(); tmpRciOutpage != _pageToDisplay.end(); ++tmpRciOutpage)
  {
    // loops on the parameters used by the outpage
    std::map< std::string, std::string >::const_iterator itParams;
    for( itParams = tmpRciOutpage->getVariablesAndValues().begin() ;
         itParams != tmpRciOutpage->getVariablesAndValues().end()  ; ++itParams )
    {
      std::string parameterValue;
      if( get_Key_Value(itParams->first, parameterValue) == 0 )
        tmpRciOutpage->setParameterValue( itParams->first,  parameterValue );
      else
        tmpRciOutpage->setParameterValue( itParams->first, "INVALID" );
    }
    tmpRciOutpage->buildText();
  }
  
  if( this == _runningPlugin)
    _nonBlocking.addFunction( boost::bind( &Rci::Plugin_Base::echoPlugin, this) );
  return ans;
}

void Rci::Plugin_Base::updatePage()
{
  _nonBlocking.addFunction( boost::bind( &Plugin_Base::p_updatePage, this) );
}

void Rci::Plugin_Base::echoPlugin()
{
  if(_daemon != NULL)
   _daemon->echoPlugin( this->_pageToDisplay, this->_sysPageTitle );
}
