/** @file plugin_base.h
 *  This file defines the base class for plugin called by the main programm
 *  @author Matthieu Charbonnier
 */

#ifndef __BIGHCI_PLUGIN_BASE_H__
#define __BIGHCI_PLUGIN_BASE_H__

#include <errno.h> // Last Error number
#include <map>
#include <vector>
#include <boost/thread.hpp>
#include <boost/system/error_code.hpp>
#include <boost/any.hpp>
#include <logcplusplus.hpp>
#include "rciDaemonBase.h"
#include "rcCode.h"
#include "rciOutpage.h"
#include "nonBlockingClass.h"

namespace Rci
{
typedef std::string Plugin_Data_Element;
typedef std::map<std::string, Plugin_Data_Element> Plugin_Data;
typedef std::map<std::string, Plugin_Data_Element>::iterator It_Plugin_Data;
/** @brief This class defines a base plugins for the daemon
 *
 *  include this file to create your own plugins
 */
class Plugin_Base
{
public:

  typedef std::string PluginValue; ///< this will probably be replaced by a variant type.
  static Rci::Plugin_Base* createPlugin(const std::string &);  ///< loads a plugin

protected:
  NonBlockingClass _nonBlocking; ///< plugin non blocking class

private:
  static std::vector<std::string> _keys;   ///< Common keys
  static std::vector<std::string> _status; ///< Common Status

  static Plugin_Base* _runningPlugin; ///< Pointer to the current running plugin
  static boost::mutex _mutexPlugin;   ///< Protects the plugin_base data

  static RciDaemonBase* _daemon;  ///< Daemon to use to communicate with plugins

  void *_pluginHandler;       ///< stores the file descriptor of the plugin shared object  
  
  /////////////////////
  // Displaying Data //
  /////////////////////
  std::list<RciOutpage> _pageToDisplay;   ///< contains the elements to display on the page.
  std::list<RciOutpage> _systemPage;      ///< contains the elements to display on the system page.
  const std::string _systemName;          ///< name of the system page to use
  bool _displaySysPage;                   ///< true if the systemPage must be drawn
  timeval _sysPageLifeInit;               ///< base value of the system Page life time (ms)

  struct Command { std::string translatorName; std::string actionName; };
  std::map<RcCode, std::string> _actionsMap; ///< stores every command associated with an action and translator.

  void initPlugin()
  {
    gettimeofday(&_lastTime, NULL);
    _defaultInterval.tv_sec = 0;
    _defaultInterval.tv_usec = 200000;
    _timeInterval = _defaultInterval;

    _sysPageLifeInit.tv_sec = 1;
    _sysPageLifeInit.tv_usec = 0;
  }

  typedef void (*Eraser) ( Rci::Plugin_Base* );
  void (*eraser) ( Rci::Plugin_Base* );

  bool _has_Changed;          ///< Indicates that a plugin has up to date
  bool _requestSysPage;       ///< True if the plugin needs to display system datas (volume...)
  std::string _current_State; ///< Stores the current state of the programm concerned by the plugin
  std::string _plugin_Name;   ///< Name of the plugin
  std::string _sysPageTitle;  ///< Title of the system Page when displayed
  PluginValue _sysValue;      ///< Value displayed on the system Page
  
  std::string _lastState;     ///< used to check wether or not the action must be repeated
  timeval _lastTime;          ///< last time the action was performed
  timeval _timeInterval;      ///< interval at which an action must be performed
  timeval _defaultInterval;   ///< default time between 2 identical actions.
  
  boost::mutex _mutex_Base;   ///< Protects the plugin_base datas

protected:
  /** type of function called by go_to_state */
  typedef boost::function<void ( bool, CmdArguments, std::string*, CmdAnswer* )> GoToStateFunction;
  boost::system::error_code _error_Code;  ///< Error code, set up if an error occured
  LgCpp::Logger _logger; ///< plugin logger

  std::map< std::string, GoToStateFunction > _states; ///< states to go to and associated function

public:
  /// plugin constructor
  Plugin_Base( const std::string& );
  Plugin_Base( const std::string&, void (*)( Rci::Plugin_Base* ) );
  /// plugin destructor
  virtual ~Plugin_Base()
  {
    clearRunning();
    _logger.logIt( "Removing Plugin", LgCpp::Logger::logINFO);
  }
  
  void addAsyncAction(boost::function<void (void)>& inFunction)
  {
    _nonBlocking.addFunction(inFunction);
  }
  
  void setEraser( Eraser inEraser ){eraser = inEraser;}
  Eraser getEraser(){return eraser;}
  
  static void setDaemon( RciDaemonBase* inDaemon )
  { _daemon = inDaemon; }

  /** returns the capabilities of the plugin
   *  @param[out] outCapabilities : capabilities of the plugin
   */
  void getCapabilities( std::vector<std::string> outCapabilities )
  {
    outCapabilities.resize( _states.size(), "" );
    
    std::map< std::string, GoToStateFunction >::iterator itState;
    int i = 0;
    for( itState = _states.begin(); itState != _states.end(); ++itState, ++i )
      outCapabilities[i] = itState->first;
  }
  
  /** Add a data to display to the plugin.
   *  @param[in] inData: formula for the data
   *  @param[in] inDataAction: Identifier of the action corresponding to this data. Provides a way
   *    for the user to interact with this data.
   *  @return 0
   */
  int addDataToDisplay(const std::string &inData, const std::string &inDataAction)
  {
    RciOutpage outPage( inData, inDataAction );
    _pageToDisplay.push_back(outPage);
    if( getRunning() == this)
      echoPlugin();

    return 0;
  }
/////////////////////////////////////////
//      Status Names                   //
/////////////////////////////////////////
  /** This enum defines default status used by the daemon to obtain information about the playing programm.
   *  Plugin specific status must be defined in the Plugin config file.
   */
  enum Satus_Id
  {
    Playing,    ///< The programm is playing
    Idle,       ///< The programm is idle
    Number_Of_Status ///< Used to determine the number of Status
  };
  
  /// returns the name of the plugin
  std::string get_Name()  
  {
    boost::unique_lock<boost::mutex> lk(_mutex_Base);
    return _plugin_Name;
  }
  
  /// return true if the system Page is required, when called the bit is cleared
  bool get_RequestSysPage()  
  {
    bool ans;
    boost::unique_lock<boost::mutex> lk(_mutex_Base);
    ans = _requestSysPage;
    _requestSysPage = false;
    return ans;
  }
  
  /** returns the value to display on the system page
   *
   *  @param[out] outValue: value of the system page
   */
  void get_SysValue( PluginValue &outValue ) 
  {

    boost::unique_lock<boost::mutex> lk(_mutex_Base);
    outValue = _sysValue;
  }
  
  /** returns the title to display on the system page
   *
   *  @param[out] outTitle: title of the system page
   */
  void get_SysTitle( std::string &outTitle ) 
  {
    
    boost::unique_lock<boost::mutex> lk(_mutex_Base);
    outTitle = _sysPageTitle;
  }

  /** Called when the plugin is started
   *
   *  This function stops the current running plugin and set _runningPlugin to this.
   */
  void setRunning()
  {
    Plugin_Base* runningPlugin = NULL;
    {
      boost::unique_lock<boost::mutex> lk(_mutexPlugin);
      runningPlugin = _runningPlugin;
    }
      
    if( runningPlugin != NULL && runningPlugin != this )
    {
      runningPlugin->stop();
      std::string message;
      message = "plugin ";
      message += runningPlugin->get_Name();
      message += " stopped by ";
      message += get_Name();
      _logger.logIt(message, LgCpp::Logger::logINFO);
    }
    {
      boost::unique_lock<boost::mutex> lk(_mutexPlugin);
      _runningPlugin = this;
      updatePage();
      _logger.logIt("plugin started", LgCpp::Logger::logINFO);
    }
  }

  /** called when the plugin is stopped
   *
   *  This function sets _runningPlugin to NULL
   */
  void clearRunning()
  {
    boost::unique_lock<boost::mutex> lk(_mutexPlugin);
    if( _runningPlugin == this )
      _runningPlugin = NULL;
    _logger.logIt("plugin stopped", LgCpp::Logger::logINFO);
  }

  /// returns the running plugin
  static Plugin_Base* getRunning()
  {
    return _runningPlugin;
  }

  ///////////////////////////
  /// Device interactions ///
  ///////////////////////////
  ////// Commands interactions
  /// Adds an action associated to an IR command to the plugin
  int addCommand(const std::string &inAction, const std::string &inTranslatorName,
                 const std::string &IRValue, CmdArguments &inArguments);
  /// Asks the plugin to treat an incomming IR command
  int handleCommand( RcCode &inCode, std::string &outAction, CmdAnswer &outAnswers );
                  
  /// find an replace parameters in a formula
  std::string findAndReplaceParams( const std::string &inFormula );

  /** @brief returns the value associated to the key
   *  @param[in]   inKey : name of the value to return
   *  @param[out]  outValue : value associated to the key
   *  @return 0 on success, -1 on failure
   */
  virtual int get_Key_Value(const std::string &inKey, std::string &outValue) = 0;

  /// Stops the plugin (for example stop playing)
  virtual void stop()          
  {
    clearRunning();
  }
  
  /// Starts the plugin (for example start playing)
  virtual void start()         
  {
    setRunning();
  }

  /** @brief set the plugin to a given state
   *  The device handles requests for setting the plugin in the state.
   *  One can use timers to avoid frequently repeated requests.
   *  @param[in] inState : name of the state to go to
   *  @param[in] inRepeat : true to force the handling of the request 
   *  @param[in] inArguments : Arguments to be passed to the plugin
   *  @param[out] outAction : Action to perform with the given answer
   *  @param[out] outAnswers : Answers of the plugin
   *  @return 0 on success, -1 on error.
   */
  virtual int go_To_State(const std::string &inState, bool inRepeat, const CmdArguments & inArguments,
                          std::string &outAction, CmdAnswer &outAnswers)
  {
    std::map<std::string, GoToStateFunction>::iterator it;
    it = _states.find(inState);
    if( it != _states.end())
    {
      it->second(inRepeat, inArguments, &outAction, &outAnswers);
      _logger.logIt("answer action: ", LgCpp::Logger::logWARNING);
      _logger.logIt(outAction, LgCpp::Logger::logWARNING);
    }
    else
      _logger.logIt("unknown state", LgCpp::Logger::logWARNING);
  }

  virtual void updatePage();  ///< echo the data of the plugin on a page
 
 
protected:
  
  /** add a possible state for the plugin
   *
   *  @param[in] inState: name of the state to add. If a state with this name already exists it will be
   *    replaced.
   *  @param[in] inFunction: function associated with the given state
   */
  void addState( std::string inState, GoToStateFunction inFunction )
  {
    _states[inState] = inFunction;
  }
  
  /// sets the name of the plugin
  void set_Name(const std::string &inName)
  {
    boost::unique_lock<boost::mutex> lk(_mutex_Base);
    _plugin_Name = inName;
  }
  
  /** sets the current state of the plugin
   *
   *  This function will not change the running state of the plugin nor call the corresponding function of
   *  the states list.
   *  @param[in] inState: actual state of the plugin
   */
  void set_Current_State(const std::string &inState )
  {
    boost::unique_lock<boost::mutex> lk(_mutex_Base);
    _current_State = inState;
    _has_Changed = true;
  }
      
  /// set the _has_Changed flag to true
  void set_Has_Changed()
  {
    boost::unique_lock<boost::mutex> lk(_mutex_Base);
    _has_Changed = true;
  }
  
  /// reuquest the system to display the Plugin system page
  void set_RequestSysPage()
  {
    boost::unique_lock<boost::mutex> lk(_mutex_Base);
    _requestSysPage = true;
  }
  
  /// Sets the text to display on the system page
  void set_SysValue( const PluginValue &inValue ) 
  {
    
    boost::unique_lock<boost::mutex> lk(_mutex_Base);
    _sysValue = inValue;
  }

  /// Sets the title of the system page
  void set_SysTitle( const std::string &inTitle ) 
  {
    
    boost::unique_lock<boost::mutex> lk(_mutex_Base);
    _sysPageTitle = inTitle;
  }

  /** sets the time interval for autorepeat
   *  @param[in] inTimeInterval : input time
   */
  void set_TimeInterval( const timeval &inTimeInterval )
  {
    _timeInterval.tv_sec = inTimeInterval.tv_sec;
    _timeInterval.tv_usec = inTimeInterval.tv_usec;
  }

  /** sets the time interval for autorepeat
   *  @param[in] inSec : number of seconds
   *  @param[in] inUSec : number of microseconds
   */
  void set_TimeInterval( time_t inSec, suseconds_t inUSec )
  {
    _timeInterval.tv_sec = inSec;
    _timeInterval.tv_usec = inUSec;
  }
  
  int p_updatePage();  ///< echo the data of the plugin on a page

  bool checkTimer( timeval &lastTime, timeval const& inInterval, bool updateIfTrue = true ); ///< timing function
//  int getDataToDisplay(std::list<RciOutpage> &outData, std::string &outPageName)

  bool checkRepeatAction( const std::string &inState, bool inRepeat ); ///< check if the action must be repeated
  
private:
  void echoPlugin();
};

}

#endif //__BIGHCI_PLUGIN_BASE_H__
