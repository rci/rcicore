#include <sstream>
#include <dbus/dbus.h>
#include "plugin_dbus.h"

DBus::BusDispatcher * Rci::Plugin_Dbus::_dispatcher = NULL;
DBus::Connection * Rci::Plugin_Dbus::_connection = NULL;
DBus::DefaultTimeout * Rci::Plugin_Dbus::_defaultTimeout = NULL;
boost::thread * Rci::Plugin_Dbus::_thrdDispatcher = NULL;
int Rci::Plugin_Dbus::_n_Running_Plugins = 0;
boost::mutex Rci::Plugin_Dbus::_serviceMapMutex;
boost::mutex Rci::Plugin_Dbus::_dispatcherMutex;
std::map<std::string, std::string> Rci::Plugin_Dbus::_serviceMap;
bool Rci::Plugin_Dbus::_dispatcherStop = false;
Rci::FreedesktopServer * Rci::Plugin_Dbus::_dbusServer = NULL;

/** This function is called when the dbus signal "NameOwnerChanged" is triggered
 *  @param[in] name: name of the service
 *  @param[in] oldOwner: previous owner of the service
 *  @param[in] newOwner: new owner of the service
 */
void Rci::FreedesktopServer::NameOwnerChanged(const std::string &name, const std::string &oldOwner,
                      const std::string &newOwner)
{
  boost::unique_lock<boost::mutex> lk(Plugin_Dbus::_serviceMapMutex);
  std::map<std::string, std::string>::iterator itServiceMap = Plugin_Dbus::_serviceMap.find(name);
  if( itServiceMap != Plugin_Dbus::_serviceMap.end() )
    itServiceMap->second = newOwner;
}

void Rci::FreedesktopServer::NameAcquired(const std::string &name)
{;}

void Rci::FreedesktopServer::NameLost(const std::string &name)
{;}

Rci::Plugin_Dbus::Plugin_Dbus(std::string inName) :
        Plugin_Base(inName), _connectionPlugin(NULL)
{
  _owner = _serviceMap.end();
  ++_n_Running_Plugins;
  
  if(_n_Running_Plugins == 1)
  {
    dbus_threads_init_default();
    
    // Creating the DBus Dispatcher
    _dispatcher = new DBus::BusDispatcher();
    DBus::default_dispatcher = _dispatcher;
    
    // Making a connection for DBUS Proxies
    _connection = new DBus::Connection( DBus::Connection::SessionBus() );
    
    // Adding a default timeout so that _dispatcher.do_iteration waits only 1ms instead of 10 000ms
    _defaultTimeout = new DBus::DefaultTimeout( 1, true, _dispatcher ) ;
    
    // Starts dispatching
    _thrdDispatcher = new boost::thread( dispatchingLoop );
    
    boost::unique_lock<boost::mutex> lk(Rci::Plugin_Dbus::_dispatcherMutex);
    _dbusServer = new FreedesktopServer( *_connection);
  }
  _connectionPlugin = new DBus::Connection( DBus::Connection::SessionBus() );
  _filter = new ::DBus::Callback<Plugin_Dbus, bool, const ::DBus::Message &>(this, &Plugin_Dbus::msgFilter);
  _connectionPlugin->add_filter(_filter);
}

Rci::Plugin_Dbus::~Plugin_Dbus()
{    
  --_n_Running_Plugins;

  _connectionPlugin->disconnect();
  
  ///TODO @todo deleting connections leads to a sigsegv in the dispatcher thread 
  //if( _connectionPlugin != NULL )
  //  delete _connectionPlugin;
  //_connectionPlugin = NULL;
  
  // if this is the last plugin, clears the dbus parameters
  if( _n_Running_Plugins == 0 )
  {
    // stops dispatching
    if(_thrdDispatcher != NULL)
    {
      _dispatcherStop = true;
      delete _thrdDispatcher;
      _logger.logIt( "dispatcher stopped", LgCpp::Logger::logINFO );
    }
    
    // close FreedesktopServer
    if( _dbusServer != NULL );
      delete _dbusServer;
    _dbusServer = NULL;
    
    // close DBUS connection
    _connection->disconnect();
    if( _connection != NULL )
      delete _connection;
    _connection = NULL;
    // removes timeout
    if( _defaultTimeout != NULL )
      delete _defaultTimeout;
    _defaultTimeout = NULL;
    // close DBUS connection
    if( _dispatcher != NULL )
      delete _dispatcher;
    _dispatcher = NULL;
  }
  else
  {
    std::ostringstream ssMessage;
    ssMessage << "Dbus plugins remaining : " << _n_Running_Plugins;
    _logger.logIt( ssMessage, LgCpp::Logger::logINFO );
  }
}

/** adds a service to the service map and try to get its current owner
 *  @param[in] inService: service to use
 */
void Rci::Plugin_Dbus::setService( const std::string &inService )
{
  std::string name = "";
  try{ name = _dbusServer->GetNameOwner(inService); }
  catch ( DBus::Error error ) { name = ""; }
  
  boost::unique_lock<boost::mutex> lk(_serviceMapMutex);
  _owner = _serviceMap.insert(std::pair<std::string, std::string>(inService, name)).first;
}

/** remove the service
 *  @param[in] inService: service to remove
 */
void Rci::Plugin_Dbus::removeService( const std::string &inService )
{
  boost::unique_lock<boost::mutex> lk(_serviceMapMutex);
    _serviceMap.erase(inService);
}

/** mainloop handling the dispatcher */
void Rci::Plugin_Dbus::dispatchingLoop()
{
    _dispatcherStop = false;
    int i;
    // Main dispatcher loop
    while (_dispatcherStop == false)
    {
      {
        // handles signals
        boost::unique_lock<boost::mutex> lk(Rci::Plugin_Dbus::_dispatcherMutex);
        _dispatcher->do_iteration();
      }
      usleep(10000);
    }
}

/** handle DBus errors
 *  @param[in] inError : dbus error to handle
 *  @note The function unlocks the dispatcher since it has been locked when the error was thrown
 */
void Rci::Plugin_Dbus::errorHandlerDBus( DBus::Error &inError )
{
    std::ostringstream ssMessage;
    ssMessage << "error at " << inError.name() << ": " << inError.message();
    _logger.logIt( ssMessage, LgCpp::Logger::logERROR);    
}


/** Stop the plugin */
void Rci::Plugin_Dbus::stop_Catch()
{
  try{ stop_Function(); }
  catch( DBus::Error error )
  {
    errorHandlerDBus(error);
  }
}

/** @param in_Variant : Variant to convert to string
 *  @return an empty string on failure, or the converted data on success
 */
std::string Rci::Plugin_Dbus::variant_2_String(const DBus::Variant in_Variant)
{
  std::string ans;
  DBus::MessageIter tmp_It;
  tmp_It = in_Variant.reader();
  std::ostringstream converter;
  // The variant is a string
  if( in_Variant.signature() == "s" )
  {
    tmp_It >> ans;
    converter << ans;
  }
  else if ( in_Variant.signature() == "as" )
  {
    std::vector<std::string> as;
    tmp_It >> as;
    int i;
    for(i = 0; i < as.size()-1; ++i)
      converter << as[i] << ", ";
    converter << as[i];
  }
  else if ( in_Variant.signature() == "y" )
  {
    uint8_t y;
    tmp_It >> y;
    converter << y;
  }
  else if ( in_Variant.signature() == "b" )
  {
    bool b;
    tmp_It >> b;
    if ( b )
      converter << "true";
    else
      converter << "false";
  }
  else if ( in_Variant.signature() == "i" )
  {
    int32_t i;
    tmp_It >> i;
    converter << i;
  }
  else if ( in_Variant.signature() == "x" )
  {
    int64_t x;
    tmp_It >> x;
    converter << x;
  }
  else if ( in_Variant.signature() == "d" )
  {
    double d;
    tmp_It >> d;
    converter << d;
  }
  else
  {
    converter << in_Variant.signature();
  }
  
  ans = converter.str();
  return ans;
}

