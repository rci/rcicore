/** @file plugin_dbus.h
 *  @brief This file defines a plugin class allowing communication over DBUS
 *  @author Matthieu Charbonnier
 */
#ifndef __BIGHCI_PLUGIN_DBUS_H__
#define __BIGHCI_PLUGIN_DBUS_H__

#define DBUS_HAS_THREADS_INIT_DEFAULT
#define DBUS_HAS_RECURSIVE_MUTEX
#define HAVE_PTHREAD 1
#define HAVE_PTHREAD_H 1

#include <stdarg.h>
#include <boost/any.hpp>
#include <boost/thread.hpp>
#include <dbus-c++-1/dbus-c++/dbus.h>
#include "plugin_base.h"
#include "org.freedesktop.DBus.h"

namespace Rci
{
/** @brief This class defines the freedesktop proxy used to determine the connection owning a service.
 *
 *  This is required to filter the signals received and let only the corresponding plugin handle them.
 */
class FreedesktopServer :
  public org::freedesktop::DBus_proxy,
  public DBus::IntrospectableProxy,
  public DBus::ObjectProxy
{
public:
  /// constructor
  FreedesktopServer(DBus::Connection &inConnection ) :
    DBus::ObjectProxy( inConnection, "/org/freedesktop/DBus", "org.freedesktop.DBus"), _logger("freedesktop")
  {}
  /// destructor
  ~FreedesktopServer() {}
  
  /// called when the owner of a connection has changed
  void NameOwnerChanged(const std::string &name, const std::string &newOwner,
                      const std::string &oldOwner);
  /// not used
  void NameAcquired(const std::string &);
  /// not used
  void NameLost(const std::string &);
  

private:
  LgCpp::Logger _logger; ///< Freedesktop Server logger
};

/** @brief This class defines a base plugin to control external program with dbus
 *
 *  @note Inherit from this class to creat your own dbus plugins.
 */
class Plugin_Dbus : public Plugin_Base
{
public:
  friend class FreedesktopServer;
  friend class RciProxy;

  static DBus::BusDispatcher *_dispatcher; ///< Dispatcher for DBus Data, \todo set to protected

  static std::string variant_2_String(const DBus::Variant in_Variant); ///< Convert a Dbus variant to string
protected:
  static int _n_Running_Plugins;         ///< Number of running plugins
  static boost::mutex _dispatcherMutex; ///< protects access to the dispatcher
  static boost::thread *_thrdDispatcher; ///< runs the dispatching loop
  static DBus::DefaultTimeout *_defaultTimeout; ///< default timeout
  DBus::Connection *_connectionPlugin;   ///< Connection used for the plugin

  ::DBus::MessageSlot _filter; ///< filter used on the DBus connection
  virtual bool msgFilter( const ::DBus::Message & msg ) 
    { return false;} ///< filters the messages
  
  static boost::mutex _serviceMapMutex; ///< mutex locking the service map
  static std::map<std::string, std::string> _serviceMap; ///< service and owner pairs
  
  void setService( const std::string &inService );  ///< set the service name
  void removeService( const std::string &inService );///< remove a service from the map
  
  std::map< std::string, std::string >::const_iterator _owner; ///< connection names and their owner

private:
  static DBus::Connection *_connection;  ///< Connection used to communicate with org.freedesktop.DBus

public:
  Plugin_Dbus(std::string inName);///< Constructor
  virtual ~Plugin_Dbus();         ///< Destructor

  /** returns the value associated to the key
   *  @param[in] inKey : Name of the value to return
   *  @param[out] outValue : Value associated to the input key
   *  @return 0 on success, -1 on error
   */
  int get_Key_Value(const std::string &inKey, std::string &outValue)  
  {
    int ans = 0;
    try{ ans = get_Key_Value_Catch(inKey, outValue); }
    catch( DBus::Error error )
    {
      errorHandlerDBus(error);
      ans = -1;
    }
    return ans;
  }

  void stop()     ///< Stops the plugin (for example stop playing)
  {
    _nonBlocking.addFunction( boost::bind(&Rci::Plugin_Dbus::stop_Catch, this) );
  }

  void start()    ///< Starts the plugin (for example start playing)
  {
    try{ start_Catch(); }
    catch( DBus::Error error )
    {
      errorHandlerDBus(error);
    }
  }

  /** go to the given state
   *  @param[in] inState: state to go to
   *  @param[in] inRepeat: true if the command has already been received
   *  @param[in] inArguments: arguments
   *  @param[in] outAction: action associated with the answer
   *  @param[in] outAnswers: answers returned by the plugin
   */
  int go_To_State( const std::string &inState, bool inRepeat, const CmdArguments & inArguments,
                   std::string & outAction, CmdAnswer &outAnswers)
  {
    int ans = 0;
    try{ ans = go_To_State_Catch( inState, inRepeat, inArguments, outAction, outAnswers ); }
    catch( DBus::Error error )
    {
      errorHandlerDBus(error);
      ans = -1;
    }
    return ans;
  }

protected:
  /** @brief returns the value associated to the key
   *  This version of get_Key_Value handles error thrown by DBus cplusplus
   *  @param[in]   inKey : name of the value to return
   *  @param[out]  outValue : value associated to the key
   *  @return 0 on success, -1 on failure
   */
  virtual int get_Key_Value_Catch( const std::string &inKey, std::string &outValue ) = 0;
  virtual void start_Catch() = 0;               ///< Starts the plugin (for example start playing)
  virtual void stop_Function() = 0;             ///< called asynchronously to stop the plugin

  /** @brief set the plugin to a given state
   *  This version of go_To_State handles error thrown by DBus cplusplus
   *  @param[in] inState : name of the state to go to
   *  @param[in] inRepeat : true to force the handling of the request 
   *  @param[in] inArguments : arguments to pass to the plugin
   *  @param[in] outAction : action associated with the answers
   *  @param[out] outAnswers : answers from the plugin
   *  @return 0 on success, -1 on error.
   */
  virtual int go_To_State_Catch( const std::string &inState, bool inRepeat,
                                 const CmdArguments & inArguments, std::string & outAction,
                                 CmdAnswer &outAnswers) = 0;

private:
  static void dispatchingLoop();  ///< loop dispatching signals
  static bool _dispatcherStop;    ///< set to true to stop the dispatcher

  void stop_Catch();              ///< Stops the plugin

  DBus::Error _dbusError; ///< error
  
  void errorHandlerDBus(DBus::Error &inError); ///< error handler
protected:
  int start_Plugin();     ///< Launch a thread which will listen to DBus
  
  static FreedesktopServer *_dbusServer; ///< dbus server Proxy
  
  boost::mutex _mutex;    ///< used to protect data against multithread;
};
}
#endif //__BIGHCI_PLUGIN_BASE_H__
