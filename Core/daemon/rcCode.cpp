#include "rcCode.h"
#include "rciDeviceBase.h"

void Rci::RcCode::swap( Rci::RcCode& a, Rci::RcCode& b)
{
  std::swap( a._code, b._code );
  std::swap( a._translatorName, b._translatorName );    ///< name of the tranlator
  std::swap( a._isNew, b._isNew );                    ///< true if the code has changed since the last call of the translator _translatorName
  std::swap( a._numberOfConsecutive, b._numberOfConsecutive );  ///< number of identical consecutive _code readed by the translator
  std::swap( a._numberOfIRDataReaded, b._numberOfIRDataReaded ); ///< number of IR data readed
  std::swap( a._deviceSpecificData, b._deviceSpecificData ); ///< data used to send back answers to the calling device.
  std::swap( a._commandArguments, b._commandArguments ); ///< arguments associated to the command  
}


bool Rci::RcCode::operator==( Rci::RcCode const& a ) const
{
  if ( ( _code == a._code ) && ( _translatorName == a._translatorName ) )
      return true;
  else
      return false;
}

bool Rci::RcCode::operator<( Rci::RcCode const& a ) const
{
  bool ans;
  if( _translatorName == a._translatorName )
    ans = stringCompare( _code, a._code );
  else
    ans = stringCompare( _translatorName, a._translatorName );
  return ans;
}

bool Rci::RcCode::operator>( Rci::RcCode const& a ) const
{
  bool ans;
  if( _translatorName == a._translatorName )
    ans = stringCompare( a._code, _code );
  else
    ans = stringCompare( a._translatorName, _translatorName );
  return ans;
}

/** alphabetical sorting of strings (credit to http://www.cplusplus.com/forum/general/3605/#msg15538)
 *  @param[in] left : first string
 *  @param[in] right : second string
 *  @return true if the first string must be positionate befor the second one.
 */
bool Rci::RcCode::stringCompare( const std::string &left, const std::string &right ) const
{
  for( std::string::const_iterator lit = left.begin(), rit = right.begin();
    lit != left.end() && rit != right.end(); ++lit, ++rit )
    if( *lit < *rit )
       return true;
    else if( *lit > *rit )
       return false;
  if( left.size() < right.size() )
    return true;
  return false;
}

/** send the answer back to the device
 *  param[in] inBackAction : action to perform with the answer
 *  param[in] inAnswer : answer to send back
 */
void Rci::RcCode::sendAnswer( std::string const& inBackAction, CmdAnswer const&inAnswer )
{
  if( _deviceSpecificData != NULL )
  {
    Rci::RciDeviceBase * device = _deviceSpecificData->getRemoteDevice();

    if( device != NULL )
      device->setAnswer( inBackAction, inAnswer, _deviceSpecificData );
  }
}

