#ifndef _RCCODE_H_
#define _RCCODE_H_

#include <string>
#include <vector>
#include "rciDeviceSpecificData.h"

namespace Rci
{

typedef std::vector<std::string> CmdAnswer;   ///< type of the answers returned to the device by a plugin
typedef std::vector<std::string> CmdArguments;///< type of the arguments sent by the device to a plugin

/** @brief Remote control command object */
class RcCode
{
private:
  std::string _code;             ///< int code value
  std::string _translatorName;    ///< name of the tranlator
  bool _isNew;                    ///< true if the code has changed since the last call of the translator _translatorName
  unsigned int _numberOfConsecutive;  ///< number of identical consecutive _code readed by the translator
  unsigned int _numberOfIRDataReaded; ///< number of IR data readed
  DeviceSpecificData * _deviceSpecificData; ///< data used to send back answers to the calling device.
  CmdArguments _commandArguments; ///< arguments associated to the command
  
public:
  
  /// constructor
  RcCode() : _deviceSpecificData(NULL), _isNew(false) {}
  /** constructor
   *  @param[in] inCode: code value
   *  @param[in] inTranslatorName: name of the translator that returned the code
   *  @param[in] inArgs: Argument of the command
   */
  RcCode( const std::string &inCode, const std::string &inTranslatorName, const CmdArguments &inArgs) :
    _code(inCode), _translatorName(inTranslatorName), _commandArguments(inArgs),
    _deviceSpecificData(NULL), _isNew(false) {}
  /** copy constructor */
  RcCode( const RcCode& other ) :
    _code(other._code), _translatorName(other._translatorName), _commandArguments(other._commandArguments),
    _deviceSpecificData(NULL), _isNew(other._isNew)
  {
    if(other._deviceSpecificData != NULL)
      _deviceSpecificData = other._deviceSpecificData->clone();
  }
  /** assignment operator */
  RcCode& operator=(const RcCode& other)
  {
    _code = other._code;
    _translatorName = other._translatorName;
    _isNew = other._isNew;
    _numberOfConsecutive = other._numberOfConsecutive;
    _numberOfIRDataReaded = other._numberOfIRDataReaded;

    if(other._deviceSpecificData != NULL)
      setDeviceSpecificData(other._deviceSpecificData->clone());
    else
      setDeviceSpecificData(NULL);

    _commandArguments = other._commandArguments;
  }
  /// destructor
  ~RcCode()
  {
    if(_deviceSpecificData != NULL )
    {
      delete _deviceSpecificData;
      _deviceSpecificData = NULL;
    }
  }
  /** swap two RcCode */
  static void swap( Rci::RcCode& a, Rci::RcCode& );
  /** equality operator */
  bool operator==( RcCode const& a ) const;
  /** comparison operator */
  bool operator<( RcCode const& a ) const;
  /** comparison operator */
  bool operator>( RcCode const& a ) const;
  
  /// alphabetical sorting of strings (credit to http://www.cplusplus.com/forum/general/3605/#msg15538)
  bool stringCompare( const std::string &left, const std::string &right ) const;
  
  /** sets the code information
   *  @param[in] inCode : code of the command
   */
  void setCode( const std::string &inCode ) {_code = inCode;}
  /** gets the code information
   *  @return code of the command
   */
  std::string getCode() const { return _code; }
   
  /** sets the translator name
   *  @param[in] inTranslatorName : name of the translator
   */
  void setTranslatorName( const std::string &inTranslatorName ) {_translatorName = inTranslatorName;}

  /** sets the device specific data
   *  @param[in] inDeviceSpecificData : device specific data
   */
  void setDeviceSpecificData( DeviceSpecificData *inDeviceSpecificData )
  {
      if( _deviceSpecificData != NULL )
      {
        delete _deviceSpecificData;
        _deviceSpecificData = NULL;
      }
      _deviceSpecificData = inDeviceSpecificData;
  }
  /** returns the device specific data*/
  DeviceSpecificData* getDeviceSpecificData(){ return _deviceSpecificData; }
  
  /** sets the number of consecutive codes
   *  @param[in] inNumberOfConsecutive : number of consecutive data
   */
  void setNumberOfConsecutive( int inNumberOfConsecutive ){ _numberOfConsecutive = inNumberOfConsecutive; }
  /** gets the number of consecutive codes
   *  @return number of consecutive codes
   */
  int getNumberOfConsecutive() const { return _numberOfConsecutive; }
  
  /** sets the number of IR data readed
   *  @param[in] inNumberOfIRDataReaded : number of IR data readed
   */
  void setNumberOfIRDataReaded( int inNumberOfIRDataReaded )
    { _numberOfIRDataReaded = inNumberOfIRDataReaded; }
  /** gets the number of IR data readed
   *  @return number of IR data readed
   */
  int getNumberOfIRDataReaded() const { return _numberOfIRDataReaded; }
  
  /** set is new
   *  @param[in] inIsNew : set to true if the code is a new value
   */
  void setIsNew( bool inIsNew ){ _isNew = inIsNew; }
  /** return the isNew flag*/
  bool getIsNew() const {return _isNew;}
  
  /** sets the command arguments
   *  @param[in] inArguments : value to set
   */
  void setCommandArguments( CmdArguments inArguments ) { _commandArguments = inArguments; }
  /** adds an argument to the command
   *  @param inArgument: argument to add
   */
  void addArgument( std::string inArgument ){ _commandArguments.push_back( inArgument ); }
  /** returns the arguments of the commands*/
  CmdArguments getCommandArguments() const { return _commandArguments; }
  /** clears the argument list*/
  void clearArguments(){_commandArguments.clear();}
  /** gets the number of arguments*/
  int getArgumentsNumber(){return _commandArguments.size();}
  
  /// returns the translator name
  std::string getTranslatorName() const { return _translatorName; }
  /** sends the answer back to the device
   *  @param[in] inBackAction: action associated to the answer, tells the device what to do with the answers, or what command it is expected to send back
   *  @param[in] inAnswer: answers
   */
  void sendAnswer( std::string const& inBackAction, CmdAnswer const& inAnswer );
};
}


#endif
