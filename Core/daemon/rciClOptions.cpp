#include <iostream>
#include "rciClOptions.h"

namespace Rci
{
/** prepares the option descriptions */
void RciClOptions::init()
{
  _options.add_options()
    ("help,h", "show this help message")
    ("verbose,v", "makes the program verbose")
    ("vverbose", "makes the program even more verbose")
    ("vvverbose", "makes the program mouthy")
    ("restart", "restarts the daemon")
    ("stop", "stops the daemon")
    ("noDaemon,n", "runs in front task")
    ("config-file,c", boost::program_options::value<std::string>(), "sets a custom config file")
  ;
}

/** extracts the parameters from the input string
 *  @param[in] inArgC : number of arguments
 *  @param[in] inCommandLine : Input parameters
 *  @return negative value on error, 0 on success
 */
int RciClOptions::castCommandLine( int inArgC, char ** inCommandLine )
{
  boost::program_options::store(boost::program_options::parse_command_line
              ( inArgC, inCommandLine, _options), _inputOptions );
  boost::program_options::notify( _inputOptions );
  
  // checks all options
  if( _inputOptions.count("help") )
  {
    std::cout << _options << std::endl;
    setExit();
  }
  if( _inputOptions.count("verbose") )
  {
    setVerbosity(1);
  }
  if( _inputOptions.count("vverbose") )
  {
    setVerbosity(2);
  }
  if( _inputOptions.count("vvverbose") )
  {
    setVerbosity(3);
  }
  if( _inputOptions.count("restart") )
  {
    setRestart();
  }
  if( _inputOptions.count("stop") )
  {
    setStop();
  }
  if( _inputOptions.count("noDaemon") )
  {
    noDaemon();
  }
}

}





