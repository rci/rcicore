#include <string>
#include <boost/program_options.hpp>

#ifndef __HCICLOPTIONS_HPP__
#define __HCICLOPTIONS_HPP__

namespace Rci
{

/// class used to handle the command lines options
class RciClOptions
{
private:
  boost::program_options::options_description _options; ///< options descriction
  boost::program_options::variables_map       _inputOptions; ///< options values

  unsigned int _verbosity; ///< define the login level.
  bool _hasChanged;   ///< true if at least one parameter changed.
  bool _askRestart;   ///< true if the daemon must restart.
  bool _askStop;      ///< true if the daemon must stop.
  bool _daemon;       ///< true if the program must run as daemon
  bool _exit;         ///< true if the program must leave immediatly

public:
  /// constructor
  RciClOptions() : _options( "Rci options" ), _verbosity(0), _hasChanged(false), _askRestart(false),
          _askStop(false), _daemon(true), _exit(false)
  {
    init();
  }
  /** constructor from command line values
   *
   *  @param[in] inArgC: argc of the main function
   *  @param[in] inCommandLine: argv of the main function
   */
  RciClOptions( int inArgC, char **inCommandLine ) : _options( "Rci options" ), _verbosity(0), _hasChanged(false), _askRestart(false), _askStop(false), _daemon(true), _exit(false)
  {
    init();
    castCommandLine( inArgC, inCommandLine );
  }
  
  unsigned int getVerbosity(){return _verbosity;} ///< returns verbosity
  bool getRestart(){return _askRestart;} ///< return true for a restart command
  bool getStop(){return _askStop;} ///< return true for a stop command
  bool getDaemon(){return _daemon;} ///< return true if the programm must start as a daemon
  bool getExit(){return _exit;} ///< true to perform no action
  
  int castCommandLine( int inArgC, char **inCommandLine ); ///< extracts parameters from the input string

private:
  /** sets the verbosity
   *  @param[in] inVerbosity: verbosity level
   */
  void setVerbosity( unsigned int inVerbosity ) { _verbosity = inVerbosity; }
  
  void setRestart() {_askRestart = true;} ///< set restart to true
  void setStop() {_askStop = true;}       ///< set stop to true
  void noDaemon() {_daemon = false;}      ///< set daemon to false
  void setExit() {_exit = true;}          ///< set exit to true
  
  void init(); ///< initialize the options descriptors
};

}





#endif
