#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include "rciDaemon.h"
#include "rciOutpage.h"
#include "rcCode.h"

/// commands for plugins 
cfg_opt_t Rci::RciDaemon::_commandOpt[] =
{
  CFG_STR_LIST("arguments", "", CFGF_NONE),
  CFG_END()
};

/// translators for plugins 
cfg_opt_t Rci::RciDaemon::_translatorOpt[] =
{
  CFG_SEC("command",       _commandOpt, CFGF_TITLE | CFGF_MULTI),
  CFG_END()
};

/// actions and commands for plugins 
cfg_opt_t Rci::RciDaemon::_actionOpt[] =
{
  CFG_SEC("translator", _translatorOpt, CFGF_TITLE | CFGF_MULTI),
  CFG_END()
};

// plugin
cfg_opt_t Rci::RciDaemon::_pluginOpt[] =
{
  CFG_STR_LIST("data",           "", CFGF_NONE),
  CFG_STR_LIST("dataAction",    "", CFGF_NONE),
  CFG_SEC("action"   ,   _actionOpt, CFGF_TITLE | CFGF_MULTI),
  CFG_END()
};

// device paramters and translators
cfg_opt_t Rci::RciDaemon::_deviceOpt[] =
{
  CFG_STR("configFile",      "", CFGF_NONE),
  CFG_STR_LIST("translator", "", CFGF_NONE),	
  CFG_END()
};

// daemon options
cfg_opt_t Rci::RciDaemon::_daemonOpt[] =
{
  CFG_FUNC("include"         , cfg_include),
  CFG_SEC("device"           , _deviceOpt, CFGF_TITLE | CFGF_MULTI),
  CFG_INT("timeBetweenFrames", 5, CFGF_NONE),
  CFG_SEC("action"           , _actionOpt, CFGF_TITLE | CFGF_MULTI),
  CFG_SEC("plugin"           , _pluginOpt, CFGF_TITLE | CFGF_MULTI),
  CFG_END()
};


Rci::RciDaemon::~RciDaemon()       ///< Base destructor
{
  clearPlugins();
  unloadDevice();
  Plugin_Base::setDaemon( NULL );
}

/** @param inConfigFilePath : input configuration file path
 *  @return 0 on success, -1 on fail
 */
int Rci::RciDaemon::initDaemon(const std::string &inConfigFilePath)
{
  setConfigFilePath(inConfigFilePath);
  setState("Initializing");
  return initDaemon();    
}

void Rci::RciDaemon::preparePlugin( cfg_t *cfgPlugin, Plugin_Base* inPlugin )
{
  cfg_t *cfgAction, *cfgTranslator, *cfgCommand;
  // loads actions associated with their commands
  for( int iAction = 0; iAction < cfg_size( cfgPlugin, "action"); ++iAction )
  {
    cfgAction = cfg_getnsec( cfgPlugin, "action", iAction );
    
    for( int iTranslator = 0; iTranslator < cfg_size( cfgAction, "translator"); ++iTranslator )
    {
      cfgTranslator = cfg_getnsec( cfgAction, "translator", iTranslator );
      
      for( int iCommand = 0; iCommand < cfg_size( cfgTranslator, "command"); ++iCommand )
      {
        cfgCommand = cfg_getnsec( cfgTranslator, "command", iCommand );
        CmdArguments arguments;
        // gets the default arguments for command.
        for( int iArgument = 0; iArgument < cfg_size( cfgCommand, "arguments"); ++iArgument)
        {
          std::string argument = cfg_getnstr(cfgCommand, "arguments", iArgument);
          arguments.push_back( argument );
        }

        inPlugin->addCommand( cfg_title( cfgAction ),
                              cfg_title( cfgTranslator ),
                              cfg_title( cfgCommand ),
                              arguments);
      }
    }
  }
  _logger.logIt( "command added", LgCpp::Logger::logINFO );
}
/** initializes the daemon from the config file
 *  @return 0 on success, -1 on fail
 */
int Rci::RciDaemon::initDaemon()
{
  Plugin_Base::setDaemon( this );
  RciDeviceBase::setDaemon( this );
  
  std::ostringstream ssMessage;
  std::string errorString = "", message;
  
  cfg_t *cfg;
  cfg = cfg_init( _daemonOpt, CFGF_NONE );

  ssMessage.str("");
  ssMessage << "opening config file : " << _configFilePath;
  _logger.logIt(ssMessage, LgCpp::Logger::logINFO);

  int ans = 0;

  if(cfg_parse(cfg, _configFilePath.c_str()) == CFG_PARSE_ERROR)
  {
    ssMessage.str("");
    ssMessage << "error n°" << errno << " opening " << _configFilePath;
    _logger.logIt(ssMessage, LgCpp::Logger::logERROR);
    ans = -1;
  }
  else
  {
    // load the devices
    setState("Loading Devices");
    cfg_t *cfgDevice;
    _deviceList.reserve( cfg_size( cfg, "device" ) );
       
    for( int iDevice = 0; iDevice < cfg_size( cfg, "device" ); ++iDevice )
    {
      cfgDevice = cfg_getnsec( cfg, "device", iDevice );

      std::string deviceName = DEVICES_PATH;
      deviceName += "/";
      deviceName += cfg_title(cfgDevice);
      deviceName += ".so";

      std::string path( CONFIG_PATH );
      path += "/";
      path += cfg_getstr( cfgDevice, "configFile" );
      
      if( loadDevice( deviceName, path) < 0 )
      {
        errorString = "could not open device " + deviceName;
        _logger.logIt(errorString, LgCpp::Logger::logERROR);
      }
      else
      {
        // adds translators
        for( int iTranslator = 0; iTranslator < cfg_size( cfgDevice, "translator"); ++iTranslator)
        {
           std::string translatorName = cfg_getnstr(cfgDevice, "translator", iTranslator);
           ssMessage.str("");
           ssMessage << "adding translator : " << translatorName;
           _logger.logIt(ssMessage, LgCpp::Logger::logINFO);
           _deviceList.back().first->addTranslator(translatorName);
        }
      }
    }
       
    if( _deviceList.size() == 0)
    {
      ssMessage.str("");
      ssMessage << "No device configured";
      _logger.logIt(ssMessage, LgCpp::Logger::logERROR);
      ans = -1;
    }
    else
    {
      cfg_t *cfgElement;        

      setState("Loading Plugins");
      // creates the Daemon Plugin
      {
        boost::unique_lock<boost::mutex> lk(_mutexPrivate);
        _pluginDaemon = (Plugin_Daemon*)Plugin_Daemon::maker();
        _pluginDaemon->setDevice( (_deviceList[0].first) );
        _pluginDaemon->setDaemon( this );
        _pluginsList.push_back(_pluginDaemon);
      }
      
      preparePlugin( cfg, _pluginDaemon);
      _pluginDaemon->addDataToDisplay("#state", "");
      //Starts the plugin to display the state of the daemon on the devices
      _pluginDaemon->start();

      // if no error, loads the other plugins
      cfg_t *cfgPlugin;
                
      int errorPlugin = -1;
                
      for( int iPlugin = 0; iPlugin < cfg_size(cfg, "plugin"); ++iPlugin )
      {
        std::list<Plugin_Base*>::iterator ptrPluginIt;
        cfgPlugin = cfg_getnsec( cfg, "plugin", iPlugin );
        errorPlugin = addPlugin( cfg_title(cfgPlugin), ptrPluginIt);
        if ( errorPlugin != 0 )
        {
          errorString =  "loading plugin ";
          errorString += cfg_title(cfgPlugin);
          _logger.logIt(errorString, LgCpp::Logger::logERROR);
        }
        else
        {
          // Sets data to print
          int descriptionSize = cfg_size(cfgPlugin, "dataAction");
          for( int iData = 0, iDataAction = 0; iData < cfg_size( cfgPlugin, "data");
                                                    ++iData, ++iDataAction )
          {
            std::string dataAction("");
            if( iDataAction < descriptionSize )
              dataAction = cfg_getnstr( cfgPlugin, "dataAction", iDataAction );
              
            (*ptrPluginIt)->addDataToDisplay( cfg_getnstr( cfgPlugin, "data", iData ), dataAction );
                      
            _logger.logIt(dataAction, LgCpp::Logger::logINFO);
          }
          preparePlugin( cfgPlugin, *ptrPluginIt );
        }
      }
      setState("Ready");
    }
  }
  
  cfg_free(cfg);
  
  return ans;
}


/** @param[in] inConfigFilePath : input configuration file path
 */
void Rci::RciDaemon::setConfigFilePath(const std::string &inConfigFilePath)
{
  boost::unique_lock<boost::mutex> lk(_mutexPrivate);
  _configFilePath = inConfigFilePath;
}

/** @return the current state of the daemon
 */
const std::string& Rci::RciDaemon::getState()
{
  boost::unique_lock<boost::mutex> lk(_mutexState);
  return _state;
}
/** @param[in] the state of the daemon
 */
void Rci::RciDaemon::setState(const std::string& inState)
{
  boost::unique_lock<boost::mutex> lk(_mutexState);
  _state = inState;
  if(_pluginDaemon != NULL)
    _pluginDaemon->updatePage();
}

/** Creates a new plugin and push it into the pluginList. If a plugin with the same name is found, the operation is canceled
 *
 *  @param[in] inPluginName : name of the plugin to create
 *  @param[out] outPluginIt : iterator to the last loaded plugin
 *  @return 0 on success, -1 on failure
 */
int Rci::RciDaemon::addPlugin(const std::string &inPluginName,
                              std::list<Plugin_Base*>::iterator& outPluginIt)
{
  int ans = -1;
/*    
  std::list<RciDaemonPluginElement*>::iterator tstItPlugin;
  if( findPluginByName( tstItPlugin, inPluginName) == 0 )
  {
    std::string message("the plugin");
    message += inPluginName;
    message += "already exists";
    _logger.logIt(message, LgCpp::Logger::logWARNING);
    ans = 0;
  }
  else
  {*/
  Plugin_Base *plugin = Plugin_Base::createPlugin( inPluginName );

  if( plugin != NULL )
  {
    _pluginsList.push_front( plugin );
    _pluginNames.push_back(inPluginName);
    
    outPluginIt = _pluginsList.begin();
    ans = 0;
  }
//  }
  
  return ans;
}

void Rci::RciDaemon::echoPlugin(std::list<RciOutpage>& inDataToDisplay, std::string& inDataTitle)
{
  _asyncThread.addFunction(boost::bind(&Rci::RciDaemon::echoPage, this,
                           inDataToDisplay, inDataTitle));
}

/** Removes every plugin from the plugin list
 *  @return always 0
 */
int Rci::RciDaemon::clearPlugins()
{

  boost::unique_lock<boost::mutex> lk(_mutexPrivate);
  std::list<Plugin_Base*>::iterator itPluginList;
  std::string message("removing plugin ");
  for( itPluginList = _pluginsList.begin(); itPluginList != _pluginsList.end(); ++itPluginList)
  {
    std::string intMessage = message + (*itPluginList)->get_Name();
    _logger.logIt(intMessage , LgCpp::Logger::logINFO);
    // calls the proper destructor
    ((*itPluginList)->getEraser())(*itPluginList);
    if(_pluginDaemon == *itPluginList)
      _pluginDaemon = NULL;
    *itPluginList = NULL;
  }
  _pluginNames.clear();
  _pluginsList.clear();
  
  _logger.logIt("plugins removed", LgCpp::Logger::logINFO);
}

/** @param[out] outItPlugin : out iterator to the plugin with the corresponding name
 *  @param[in]  inPluginName : name of the plugin to find
 *  @return 0 on success, -1 if not found
 */
 /*
int Rci::RciDaemon::findPluginByName(std::list<RciDaemonPluginElement*>::iterator &outItPlugin, const std::string &inPluginName)
{
  int ans;
  bool test = false;  // true if the plugin is found
  std::string pluginName;
    
  _mutexPrivate.lock();
    std::list<RciDaemonPluginElement*>::iterator itPlugin;
    for( itPlugin = _pluginsList.begin();
         itPlugin != _pluginsList.end() && test == false;
         ++itPlugin )
    {
      // Gets the name of the plugin
      pluginName = (*(*itPlugin))->get_Name();
                
      if( pluginName == inPluginName )
      {
        test = true;
        outItPlugin = itPlugin;
      }
    }
    if(itPlugin != _pluginsList.end())
      ans = 0;
    else
      ans = -1;
  _mutexPrivate.unlock();
    
  return ans;
}*/

/** @param[in] inPath : path to the device to load
 *  @param[in] inConfig : string for the device configuration (path to a file or anything else)
 *  @return 0 on success, -1 on failure
 */
int Rci::RciDaemon::loadDevice(const std::string &inPath, const std::string &inConfig)
{
  std::ostringstream ssMessage;
  int ans = 0;
  char* error;
    
  boost::unique_lock<boost::mutex> lk(_mutexPrivate);
  
  RciDeviceBase* device = NULL;
  void* deviceHandler = NULL;
  
  // opens the device shared object
  deviceHandler = dlopen(inPath.c_str(), RTLD_LAZY);
  if( deviceHandler == NULL )
  {
      // could not open the library
      ssMessage.str("");
      ssMessage << "error n°" << dlerror() << "couldn't open the device";
      _logger.logIt(ssMessage, LgCpp::Logger::logERROR);
      ans = -1;
  }
  else
  {
      // library successfully opened
      dlerror();  // clear any previous error
      
      RciDeviceBase* (*maker)(const std::string &);
      maker = (RciDeviceBase* (*)(const std::string &))dlsym( deviceHandler, "maker" );
      error = dlerror();
      if(error != NULL)
      {
          // symbol not found
          ssMessage.str("");
          ssMessage << "error n°" << error << "invalid device \"maker\" not found";
          _logger.logIt(ssMessage, LgCpp::Logger::logERROR);
          ans = -1;
      }
      else
      {
          device = maker(inConfig);
      }
  }
  
  // add the device if it was correctly loaded
  if(ans >= 0)
      _deviceList.push_back( std::make_pair( device, deviceHandler ) );
  
    return ans;
}

int Rci::RciDaemon::unloadDevice()
{
  std::ostringstream ssMessage;
  int ans = 0;
  char* error;
  
  boost::unique_lock<boost::mutex> lk(_mutexPrivate);
  while( _deviceList.size() > 0 )
  {
    _logger.logIt("removing device", LgCpp::Logger::logINFO);
     // library successfully opened
    dlerror();  // clear any previous error
    
    void (*eraser)(RciDeviceBase*);
    eraser = (void (*)(RciDeviceBase*))dlsym( _deviceList.back().second, "eraser" );
    error = dlerror();
    if(error != NULL)
    {
      // symbol not found
      ssMessage.str("");
      ssMessage << "error n°" << error << " while removing device, eraser not found";
      _logger.logIt(ssMessage, LgCpp::Logger::logERROR);
      ans = -1;
    }
    else
    {
      eraser( _deviceList.back().first );
      //dlclose( _deviceList.back().second );
      _logger.logIt("device removed", LgCpp::Logger::logINFO);
      _deviceList.pop_back();
    }
  }
  return ans;
}

void Rci::RciDaemon::echoPage
        (std::list<RciOutpage> &inDataToDisplay, const std::string &inOutpageName)
{
  for( int iDevice = 0; iDevice < _deviceList.size(); ++iDevice )
    _deviceList[iDevice].first->echoPage( inDataToDisplay, inOutpageName);
  
  std::list<RciOutpage>::iterator itData;
  for( itData = inDataToDisplay.begin(); itData != inDataToDisplay.end(); ++itData)
  {
    itData->clearTextChanged();
  }
}

void Rci::RciDaemon::handleCode(Rci::RcCode& inCode)
{
  CmdArguments args = inCode.getCommandArguments();

  CmdAnswer answer;
  std::string backAction; // action to perform by the device with the answer
  std::list<Plugin_Base*>::iterator itPluginsList;
  for (itPluginsList = _pluginsList.begin(); itPluginsList != _pluginsList.end(); ++itPluginsList)
  {
    // Sends the command to all running plugins
    answer.clear();
    if( (*itPluginsList)->handleCommand( inCode, backAction, answer ) == 0 )
    {
      DeviceSpecificData * dev = inCode.getDeviceSpecificData();
      if( dev != NULL )
        dev->getRemoteDevice()->setAnswer( backAction, answer, dev );
    }
  }
}

