#ifndef _RCIDAEMON_H_
#define _RCIDAEMON_H_

#include <boost/thread.hpp>
#include <sys/time.h>
#include <logcplusplus.hpp>
#include <confuse.h>
#include "nonBlockingClass.h"
#include "plugin_base.h"
#include "plugin_Daemon.h"
#include "rciDaemonBase.h"
#include "rciDeviceBase.h"

#define MAX_INIT_SIZE 200

namespace Rci
{

/// Daemon object, contains the main actions performed by the program
class RciDaemon : public RciDaemonBase
{
public:
  /** constructor
   *  @param[in] inConfigFilePath: path to the configuration path
   */
  RciDaemon(const std::string &inConfigFilePath): _configFilePath(inConfigFilePath),
        _logger("RciDaemon"), _error(false), _state("Starting"), _pluginDaemon(NULL)
  {
    _error = ( initDaemon() < 0 )? true : false;
  }
  
  ~RciDaemon();       ///< Base destructor
  ///<@todo set the daemon for Plugin and Devices to NULL
    
  void setConfigFilePath(const std::string &inConfigFilePath);    ///< set the configuration file path
  
  const std::string& getState(); ///< get the state of the daemon
  
  /// return the error flag
  bool checkError()
  {
    boost::unique_lock<boost::mutex> lk(_mutexPrivate);
    return _error;
  }
  
  /// return the plugin name
  std::vector<std::string> const& getPluginsName(){return _pluginNames;}
  
  void echoPage(std::list<RciOutpage> &inDataToDisplay, const std::string &inOutpageName);
  void handleCode(Rci::RcCode& inCode);
  
protected:
  bool _error; ///< true if an error occured (to be replaced with a boost error)
  int initDaemon();   ///< Initialize the daemon
  int initDaemon(const std::string &inConfigFilePath);    ///< Initializes the daemon
  void setState(const std::string&);   ///< set the state of the daemon
  
  int addPlugin(const std::string &, std::list<Plugin_Base*>::iterator& );
      ///< Adds a plugin that the daemon will listen
  int clearPlugins(); ///< remove all plugins from the plugin list
  void echoPlugin(std::list<RciOutpage>& inDataToDisplay, std::string& inDataTitle); ///< echo the input data to the device
  
  int loadDevice(const std::string &inPath, const std::string &inConfig);  ///< creates a device with the config path
  int unloadDevice();     ///< remove the device    
  
  std::list<Plugin_Base*> _pluginsList;
  std::vector< std::pair< RciDeviceBase* , void* > > _deviceList; ///< list of devices associated with their shared object handler
      
  boost::mutex _mutexPrivate;     ///< protects private data

  std::string _configFilePath;    ///< Path the configuration file

private :
  NonBlockingClass _asyncThread;
  boost::mutex _mutexState;
  Plugin_Daemon* _pluginDaemon; ///< plugin dedicated to the daemon
  std::string _state; ///< Current state of the daemon
  void preparePlugin( cfg_t *, Plugin_Base* ); ///< configures the input plugin
  std::vector<std::string> _pluginNames; ///< list of the plugin names
  LgCpp::Logger _logger;
  static cfg_opt_t _commandOpt[], _translatorOpt[], _actionOpt[], _pluginOpt[], _deviceOpt[], _daemonOpt[];
};

}

#endif
