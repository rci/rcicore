#ifndef _RCIDAEMONBASE_H_
#define _RCIDAEMONBASE_H_

namespace Rci
{
class RciOutpage;
class RcCode;
/** Base class for the RCI daemon. It defines the two main functions of the daemon.
 */
class RciDaemonBase
{
public:
  /** Function called by a plugin to display its data.
   *  @param[in] inDataToDisplay: contains the data to display.
   *  @param[in] inDataTitle: title of the data to display.
   */
  virtual void echoPlugin(std::list<RciOutpage>& inDataToDisplay, std::string& inDataTitle) = 0;
  /** Function called by the devices when a command is issued
   *  @param[in] inCode: code that the daemon must dispatch.
   */
  virtual void handleCode(RcCode& inCode) = 0;
};

}

#endif
