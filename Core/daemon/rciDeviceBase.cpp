#include "rciDeviceBase.h"

Rci::RciDaemonBase* Rci::RciDeviceBase::_daemon = NULL;

/** adds a translator with name inTranslator
 *  @param[in] inTranslatorName : name of the translator to use
 *  @return 0 on success -1 on error, 1 if the translator is already loaded
 */ 
int Rci::RciDeviceBase::addTranslator(const std::string &inTranslatorName)
{
    int ans = 0;
    int i;
    if(findTranslatorByName(i, inTranslatorName) == 0)
    {
        // the translator is already loaded
        ans = 1;
    }
    else
    {
        _vecTranslators.push_back( new RciTranslator(inTranslatorName) );
    }
    return ans;
}

/** removes every translators in the vector list
 */
void Rci::RciDeviceBase::clearTranslators()
{
    int i;
    while( _vecTranslators.size() != 0 )
    {
        if( _vecTranslators.back() != NULL )
        {
            delete _vecTranslators.back();
        }
        _vecTranslators.pop_back();
    }
}

/** @param[out] outIndex : index of the plugin if found
 *  @param[in]  inTranslatorName : name of the plugin to find
 *  @return 0 if found, -1 otherwise.
 */
int Rci::RciDeviceBase::findTranslatorByName(int &outIndex, const std::string &inTranslatorName)
{
    int ans = -1;
    outIndex = _vecTranslators.size();
    for(int i = 0; ( i < _vecTranslators.size() ) && ( ans == -1 ); ++i)
    {
        if( (*(_vecTranslators[i]))->getName() == inTranslatorName)
        {
            ans = 0;
            outIndex = i;
        }
    }
    return ans;
}


