#ifndef _HCIDEVICEBASEPARAMETERS_H_
#define _HCIDEVICEBASEPARAMETERS_H_

#include <vector>
#include <boost/thread.hpp>
#include <logcplusplus.hpp>
#include <boost/system/error_code.hpp>
#include "rciDaemonBase.h"
#include "rciOutpage.h"
#include "rciTranslator.h"
#include "rcCode.h"
#include "rciDeviceSpecificData.h"

namespace Rci
{

/** @brief This class defines a basic device
 *
 *  include this file to make your own devices.
 *  @todo The screen parameters should be moved in the rciDeviceMK1. As well as the backlight state.
 */
class RciDeviceBase
{
private:
    static RciDaemonBase* _daemon;  ///< Daemon to use to communicate with plugins
    
protected:
    std::vector<RciTranslator*> _vecTranslators;///< Translators used by the device

    std::list<unsigned int>     _IR_Data;       ///< buffer storing the IR data received

    LgCpp::Logger _logger;  ///< device logger
    std::string _configPath;        ///< Path to configuration file of the plugin
    boost::system::error_code _error;           ///< Stores the last error
  
    void sendCommand( Rci::RcCode& inCode )
    {
      if(_daemon != NULL)
        _daemon->handleCode( inCode );
    }

public:
    /** constructor
     *  @param[in] inConfigPath: configuration file path
     *  @param[in] deviceName: name of the device
     */
    RciDeviceBase(const std::string &inConfigPath, const char* deviceName = "RciDeviceBase") :
        _logger(deviceName), _configPath(inConfigPath) {;}
    /// destructor
    virtual ~RciDeviceBase(){clearTranslators();}
    
    static void setDaemon( RciDaemonBase* inDaemon )
    { _daemon = inDaemon; }
    
    /// commands to send to the screen
    enum ScreenCommands 
    {
        _clear = 0,
        _cursorOn,
        _cursorOff,
        _numberOfCommands
    };
    
    int addTranslator(const std::string &inTranslator); ///< sets the translator to use with the incomming IR data
    void clearTranslators();    ///< remove all translators
    virtual int translate( Rci::RcCode &outCmd) = 0; ///< returns a command and the name of the translator
    
    virtual int echoPage(const std::list<RciOutpage> &inDataToDisplay, const std::string &inOutpageName) = 0;
                                ///< echo the data with the given input data
    virtual void setAnswer( std::string const& inBackAction, CmdAnswer const& inAnswer,
                            DeviceSpecificData *inSpecificData) = 0; ///< called when a command is answered by the running plugin
    
private:
    int findTranslatorByName(int &outIndex, const std::string &inTranslatorName); ///< finds a translator with a certain name in the loaded translator list
};
}

#endif
