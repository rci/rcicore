#ifndef _HCIDEVICESPECIFICDATA_H_
#define _HCIDEVICESPECIFICDATA_H_


namespace Rci
{
class RciDeviceBase;

/** @brief Class used to identify the device that issued the command.
 *
 *  @note include this class file with your own device to define its specific data (if you need).
 *
 *  Since this programm handles a bidirectional communication with the remote controller, we sometimes need
 *  to identify the device that sent a command to be able to answer. This is the goal of this class.
 *  Each command received by the daemon is therefore associated with a DeviceSpecificData object that
 *  identifies the device and that can store some device specific data.
 *
 *  As an example we use it with the bluetooth4rcid device to store the bluetooth socket that emitted the
 *  command.
 */
class DeviceSpecificData
{
private:
  RciDeviceBase* _remoteDevice;  ///< device that send the command

public:
  /// base constructor
  DeviceSpecificData( RciDeviceBase* inRemoteDevice ) : _remoteDevice(inRemoteDevice) {}
  
  /// copy constructor
  DeviceSpecificData( const DeviceSpecificData& other ) : _remoteDevice(other._remoteDevice){}
  
  /// returns a copy of the object.
  virtual DeviceSpecificData& operator=( const DeviceSpecificData& other)
  {
    _remoteDevice = other._remoteDevice;
  }
  
  virtual ~DeviceSpecificData() {}
  
  /// returns a copy of the object.
  virtual DeviceSpecificData* clone() const
  {
    return new DeviceSpecificData(*this);
  }
  
  /// returns the remote device associated to these data.
  RciDeviceBase* getRemoteDevice() {return _remoteDevice;}
};
}

#endif
