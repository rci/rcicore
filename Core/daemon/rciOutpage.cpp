#include "rciOutpage.h"

/** @param[in] inName : name of the parameter
 *  @param[in] inParameterValue : value corresponding to the parameter
 *  @todo handle only names that are used in the formula
 */
void Rci::RciOutpage::setParameterValue(const VName &inName , const VValue &inValue)
{
  if( _variablesAndValues[inName] != inValue )
  {
    _variablesAndValues[inName] = inValue;
    _dataChanged = true;
  }
}

void Rci::RciOutpage::makeTextBuilder()
{
  _variablesAndValues.clear();
  _textBuilder.clear();
  
  std::string parameterName, constText;
  int lastPos = 0;
  
  int endScan = _formula.size() - 1;
  
  for(int i = 0; i < endScan; ++i)
  {
    if( _formula[i] == '#' )
    {
      constText = _formula.substr( lastPos, i - lastPos );
      
      bool end = false, getParam = false;
      int start = i + 1, j = 0;
      for( j = start; ( j < _formula.size() ) && end == false; ++j )
      {
        if( _formula[j] == ' ' ) 
        {
          parameterName = _formula.substr( start, j - start );
          end = true;
          getParam = true;
        }
        else if( j == endScan )
        {
          parameterName = _formula.substr( start, j - start + 1 );
          ++j;
          getParam = true;
        }

        if( getParam )
        {
          _textBuilder.push_back( std::pair<std::string, std::string>(constText, parameterName) );
          _variablesAndValues[parameterName] = "";
          lastPos = j + 1;
          i = lastPos;
        }
      }
    }
  }
  if( lastPos < _formula.size() )
  {
    constText = _formula.substr( lastPos, _formula.size() - lastPos );
    _textBuilder.push_back( std::pair<std::string, std::string>(constText, "") );
  }
  _dataChanged = true;
}

void Rci::RciOutpage::buildText()
{
  if( _dataChanged )
  {
    _fullText.clear();
    std::list< std::pair<CText, VName> >::iterator itTextBuilder = _textBuilder.begin();
    for( itTextBuilder = _textBuilder.begin(); itTextBuilder != _textBuilder.end(); ++itTextBuilder )
    {
      _fullText += itTextBuilder->first;
      _fullText += _variablesAndValues[itTextBuilder->second];
    }
    _dataChanged = false;
    _textChanged = true;
  }
}



