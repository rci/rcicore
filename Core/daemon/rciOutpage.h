/** @file rciOutpage.h
 *
 *  @todo change the name of the class to pluginData
 */

#ifndef _HCIOUTPAGE_H_
#define _HCIOUTPAGE_H_

#include <string>
#include <list>
#include <map>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

namespace Rci
{
/** @brief This class stores the parameter name and value as well as the action that can be associated to it.
 */
class RciOutpage
{
public:
  typedef std::string VName;  ///< name of a variable
  typedef std::string VValue; ///< value of a variable
  typedef std::string CText;  ///< constant text

private:
  std::string _formula; ///< base formula to build the text
  std::string _action;  ///< action associated with the text
  std::string _fullText;///< text of the outpage
  bool _dataChanged;    ///< true if the data used to build the text have changed
  bool _textChanged;    ///< true if the text has changed.
  
  std::map< VName, VValue > _variablesAndValues;
  std::list< std::pair<CText, VName> > _textBuilder;
  
public:
  /// constructor
  RciOutpage() : _formula(""), _action(""), _dataChanged(false), _textChanged(false) {;}
  
  /// constructor
  RciOutpage(std::string &inFormula, std::string &inDescription) :
        _formula(inFormula), _action(inDescription),
        _dataChanged(false), _textChanged(false)
        { makeTextBuilder();}

  /// constructor
  RciOutpage(std::string inFormula, std::string inDescription) :
        _formula(inFormula), _action(inDescription),
        _dataChanged(false), _textChanged(false)
        { makeTextBuilder();}

  /// constructor
  RciOutpage(std::string inFormula ) :
        _formula(inFormula), _action(""),
        _dataChanged(false), _textChanged(false)
        { makeTextBuilder();}
  
  ~RciOutpage(){;}
  
  /** sets the formula used to calculate the text
   *  @param[in] inFormula: formula to use
   */
  void setFormula(const std::string &inFormula)
    {
      _formula = inFormula;
      makeTextBuilder();
    }
  void setAction( const std::string &inAction ){_action = inAction;} ///< Sets the action associated with the text
  
  void setParameterValue(const VName &inName , const VValue &inValue); ///< sets a parameter value
  const std::map< VName, VValue >& getVariablesAndValues() const {return _variablesAndValues;}
  
  void clearTextChanged(){_textChanged = false;}
  
  /// returns the parameter name
  std::string const& getFormula() const
  {
    return _formula;
  }
  /// returns the action associated to the parameter
  std::string const& getAction() const
  {
    return _action;
  }
  /// returns the parameter value
//  std::string const& const_GetText() const;

  void buildText(); ///< builds the text to display.

  /// returns the calculated text
  std::string const& getText() const
  {
    return _fullText;
  }

  /// get the changed value
  bool constGetChanged() const
  {
    return _textChanged;
  }

private:
  void makeTextBuilder(); ///< prepare the input formula.
  /// sets the change value
  /*
  void setChanged( bool inValue )
  {
    _parameterValueChanged = inValue;
  }
  /// true if the parameter value has changed.
  bool getChanged() 
  {
    bool ans = _parameterValueChanged;
    _parameterValueChanged = false;
    return ans;
  }
  */
};
}

#endif
