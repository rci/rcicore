#include <dlfcn.h>
#include <iostream>
#include <sstream>
#include "rciTranslator.h"

Rci::RciTranslator::RciTranslator() : _handler(NULL), _translator(NULL), _logger("RciTranslator") {;}

Rci::RciTranslator::RciTranslator(const std::string &inTranslatorName) : _handler(NULL), _translator(NULL),
                                                                         _logger(inTranslatorName)
{
    init(inTranslatorName);
}

Rci::RciTranslator::~RciTranslator()
{
  if( (_translator != NULL) && (_handler != NULL) )
  {
    _eraser(_translator);
    dlclose(_handler);
    _handler = NULL;
    _translator = NULL;
  }
}

/** loads a translator from its namespace
 *  @param[in] inTranslatorName : name of the translator to open.
 */
void Rci::RciTranslator::init(const std::string &inTranslatorName)
{
    int ans = 0;
    
    // calculates the translator path
    char* error = NULL;
    std::ostringstream ssMessage;

    std::string tmpTrPath(TRANSLATORS_PATH);
    tmpTrPath += "/";
    tmpTrPath += inTranslatorName;
    tmpTrPath += ".so";
    
    _handler = dlopen(tmpTrPath.c_str(), RTLD_LAZY);
    if(_handler == NULL)
    {
        // could not open the library
        ssMessage.str("");
        ssMessage << "error n°" << dlerror() << " could not open the library: " << tmpTrPath;
        _logger.logIt(ssMessage, LgCpp::Logger::logERROR);
        ans = -1;
    }
    else
    {
        // library successfully opened
        dlerror();  // clear any previous error
        _maker = (Rci::IrTranslator_Base* (*)())dlsym(_handler, "maker");
        error = dlerror();
        if(error != NULL)
        {
            // symbol not found
            ssMessage.str("");
            ssMessage << "error n°" << error << " symbol \"maker\" not found";
            _logger.logIt(ssMessage, LgCpp::Logger::logERROR);
            ans = -1;
        }
        
        _eraser = (void (*)(Rci::IrTranslator_Base*))dlsym(_handler, "eraser");
        error = dlerror();
        if(error != NULL)
        {
            // symbol not found
            ssMessage.str("");
            ssMessage << "error n°" << error << " symbol \"eraser\" not found";
            _logger.logIt(ssMessage, LgCpp::Logger::logERROR);
            ans = -1;
        }
    }
    
    if(ans == -1)
    {
        _handler = NULL;
    }
    else
    {
        _translator = _maker();
    }
}

/** Reads n incomming IR code as long are they are identical.
 *  @param[in] inPreparedData : data to read
 *  @param[in] inNCodes : Maximum number of codes to read
 *  @param[out] outCode : decoded value
 *  @return the status of the translation
 */
Rci::IrTranslator_Base::TranslateState Rci::RciTranslator::translate(
                                    const std::vector<Rci::IrTranslator_Base::IrCode> &inPreparedData,
                                    unsigned int inNCodes, RcCode &outCode)
{
    unsigned int nCodeReaded;
    Rci::IrTranslator_Base::TranslateState ans;
    
    bool decode = true;
    unsigned int startPosition = 0;
    Rci::IrTranslator_Base::TranslateState trState;

    outCode.setNumberOfIRDataReaded( 0 );
    
    for ( nCodeReaded = 0; ( nCodeReaded < inNCodes ) && decode; ++nCodeReaded )
    {
        trState = _translator->translate( inPreparedData, outCode.getNumberOfIRDataReaded() );
        ans = trState;
        
        if(trState == Rci::IrTranslator_Base::_dataOk)
        {
            if( nCodeReaded == 0 )
            {
                outCode.setTranslatorName( _translator->getName() );
                outCode.setCode( _translator->getCode() );
                outCode.setNumberOfConsecutive( 1 );
                outCode.setIsNew( !_translator->getRepeated() );
                outCode.setNumberOfIRDataReaded( _translator->getStopPosition() );
            }
            else
            {
                if( outCode.getCode() == _translator->getCode() )
                {
                    int tmp = outCode.getNumberOfConsecutive();
                    outCode.setNumberOfConsecutive( ++tmp );
                    outCode.setNumberOfIRDataReaded(
                      outCode.getNumberOfIRDataReaded() + _translator->getStopPosition());
                }
                else
                {
                    decode = false;
                }
            }
        }
        else
        {
            if( nCodeReaded == 0 )
            {
                outCode.setNumberOfIRDataReaded( _translator->getStopPosition() );
                ans = trState;
            }
            else
            {
                ans = Rci::IrTranslator_Base::_dataOk;
            }
            decode = false;
        }
        
        // stop decoding if the last data has been reached.
        if( outCode.getNumberOfIRDataReaded() >= inPreparedData.size() )
        {
            decode = false;
        }
    }
    return ans;
}



