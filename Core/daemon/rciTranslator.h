#ifndef _HCITRANSLATOR_H_
#define _HCITRANSLATOR_H_

#include <logcplusplus.hpp>
#include "rcCode.h"
#include "irtranslator_base.h"

namespace Rci
{
/// translator container
class RciTranslator
{
public:
    /// constructor
    RciTranslator();
    /// copy constructor
    RciTranslator(const std::string &inTranslatorName);
    /// destructor
    ~RciTranslator();
    
    /// arrow operator
    IrTranslator_Base * operator -> () const
    {
        return _translator;
    }
    
    /// Try to read a certain number of identical codes on the translator.
    Rci::IrTranslator_Base::TranslateState translate(
                                  const std::vector<Rci::IrTranslator_Base::IrCode> &inPreparedData,
                                  unsigned int inNCodes, RcCode &outCode);
    
    void init(const std::string &inTranslatorName);
    
protected:
    LgCpp::Logger _logger;  ///< translator logger
    
private:

    IrTranslator_Base* _translator;
    void* _handler;
    IrTranslator_Base* (*_maker)(void);
    void (*_eraser)(IrTranslator_Base*);
};
}
#endif
