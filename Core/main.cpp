#include <signal.h>
#include <logcplusplus.hpp>
#include <config.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <execinfo.h>
#include <cxxabi.h>

#include "daemon/rciClOptions.h"
#include "daemon/daemonize.h"
#include "daemon/rciDaemon.h"

bool gRun;
Rci::RciDaemon * defaultDaemon;

void niam( int sig )
{
  
  if(sig == SIGSEGV)
  {
    LgCpp::Logger logger( "critical" );
    
    int maxSize = 64;
    void * buffer[maxSize];
    
    int nFct = backtrace(buffer, maxSize);
    {
      std::ostringstream ssMessage;
      ssMessage << nFct << " values in the stack trace" << std::endl;
      logger.logIt(ssMessage, LgCpp::Logger::logCRITICAL);
    }
    
    // getting function descriptions
    char **fctDescriptions = backtrace_symbols(buffer, nFct);
    if(fctDescriptions == NULL)
    {
      logger.logIt("No description available", LgCpp::Logger::logCRITICAL);
    }
    else
    {
      for(int i = 0; i < nFct; ++i)
      {
        if(fctDescriptions[i] != NULL)
          logger.logIt(fctDescriptions[i], LgCpp::Logger::logCRITICAL);
      }
    }
    exit(-1);
  }
  else if(sig != SIGALRM)
    gRun = false;

}

LgCpp::Logger::Severity calcSeverity( unsigned int inVerbosity )
{
  LgCpp::Logger::Severity logSev = LgCpp::Logger::logDEBUG;
  switch( inVerbosity )
  {
    case(0):
      logSev = LgCpp::Logger::logERROR;
      break;
    case(1):
      logSev = LgCpp::Logger::logWARNING;
      break;
    case(2):
      logSev = LgCpp::Logger::logINFO;
      break;
    case(3):
    default:
      logSev = LgCpp::Logger::logDEBUG;
      break;
  }
  return logSev;
}

int main( int argc, char **argv )
{
  
  signal( SIGTERM, niam );
  signal( SIGINT, niam );
  signal( SIGALRM, niam );
  signal( SIGSEGV, niam );

  LgCpp::Sink stdOutput( std::cout );
  

  LgCpp::Logger logger( "main" );

  // gets the command line parameters
  Rci::RciClOptions options( argc, argv );
  
  if( !options.getExit() )
  {
    Rci::Daemonize daemonizer( "rciDaemon", WORKING_PATH );
    
    LgCpp::Logger::Severity logSev = calcSeverity( options.getVerbosity() );
    stdOutput.setLogSeverity( logSev );
    
    if( options.getRestart() || options.getStop() )
    {
      std::string message = options.getRestart() ? "restart" : "stop";
      
      if( daemonizer.post( message ) == 0)
        logger.logIt( "message sent", LgCpp::Logger::logINFO );
      else
        logger.logIt( "rciDaemon is not running", LgCpp::Logger::logERROR );
    }
    else
    {
      bool ok = true;

      bool daemon = options.getDaemon();
      ok = ( daemonizer.daemonizeMe( !daemon ) >= 0 )? true : false;

      struct passwd *pw = getpwuid(getuid());
      
      std::string logPath;
      if(pw != NULL)
      {
        const char *homedir = pw->pw_dir;
        logPath = homedir;
      }
      else
        logPath = ".";
        
      std::string logFile( logPath );
      logFile+="/.rciDaemon.log";
      LgCpp::Sink logOutput( logFile );
      logOutput.setLogSeverity( logSev );


      if( ok )
      {
        std::string path(CONFIG_PATH);
        path += "/daemon.conf";

        gRun = true;
        {
          std::ifstream configFile(path.c_str());
          if (!configFile.good())
          {
            std::string examplePath(CONFIG_PATH);
            examplePath += "/template-daemon.conf";
            std::ostringstream msg;
            msg << "No configuration file " << std::endl << path << std::endl
                << "Write one with " << std::endl << examplePath << std::endl << " as an example.";
            logger.logIt( msg, LgCpp::Logger::logCRITICAL );
            gRun = false;
          }
        }

        while(gRun)
        {
          Rci::RciDaemon* rciDriver = new Rci::RciDaemon( path );
          
          if( rciDriver->checkError() )
          {
            logger.logIt( "an error occured", LgCpp::Logger::logERROR );
            gRun = false;
          }
          
          std::string test;
          bool restart = false;
          while( gRun && !restart)
          {
            daemonizer.read(test);
            if( daemonizer.getStop() )
            {
              logger.logIt( "stop requested", LgCpp::Logger::logINFO );
              gRun = false;
            }
            else if( daemonizer.getRestart() )
            {
              logger.logIt( "restart requested", LgCpp::Logger::logINFO );
              restart = true;
            }
            else
              sleep( 100 ); // sleep 100s or until a signal arrives.
          }
          delete rciDriver;
        }
      }
      else
        logger.logIt( "rciDaemon is already running", LgCpp::Logger::logERROR );
    }
  }
  return 0;
}
